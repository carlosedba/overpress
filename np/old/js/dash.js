var Dash = (function () {
	'use strict';

	var doc = document,
	    OverElement = doc.registerElement('app-overpress', { prototype: Object.create(HTMLElement.prototype) }),
        SlidesElement = doc.registerElement('app-slides', { prototype: Object.create(HTMLElement.prototype) }),
        SlideElement = doc.registerElement('app-slide', { prototype: Object.create(HTMLElement.prototype) }),
        API = {
        	hu3: function () {
        		console.log('hu3!');
        	}
        };

    var lmenu = doc.querySelector('.lmenu'),
        navbar = doc.querySelector('.navbar');

    function leftMenuT () {
    	lmenu.classList.toggle('lmenu-open');
    	navbar.classList.toggle('navbar-toright');
    }

    //On Clicks

    var barz = doc.querySelector('.barz');

    barz.onclick = function () {
    	leftMenuT();
    }

    // API
    return API;

})();