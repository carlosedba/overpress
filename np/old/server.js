var express = require('express');
var app = express();
var router = express.Router();
var fs = require('fs');
var path = require('path');
var port = 116;

//Router
app.use('/', router);

router.use(function (req, res, next) {
    console.log(req.method, req.url);
    next();
});

router.get('/', function (req, res) {
    res.sendfile('index.html');
});

router.get(/^(.+)$/, function (req, res) {
    res.sendfile(__dirname + req.params[0]);
});

server = app.listen(port, function () {
    console.log('HTTP listening on port ' + port);
});