// *************************************************** //
// * Overpress Client Core *************************** //
// * Alpha Stage ************************************* //
// * Developed by Carlos Eduardo Barbosa de Almeida  * //
// *************************************************** //

// Variável Global da API
Overpress = window.Overpress || {

	//Início da API
	API : {

		next: function(){
			document.write('next');
			return 'Operação "next" Executada!';
		},
		prev: function(){
			document.write('prev');
			return 'Operação "prev" Executada!';
		}

	}

};