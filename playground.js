db.createUser(
   {
     user: "CarlosEDBA",
     pwd: "%Nm96pD?o",
     roles:
       [
         "clusterAdmin",
         { db: "overpress", role: "readWrite" }
       ]
   }
)

db.runCommand( { updateUser : "CarlosEDBA",
                 pwd : "CarlosAlmeida",
                 roles : [
                           "clusterAdmin",
                           { db: "overpress", role: "readWrite" }
                         ]
             } )

var userSchema = new Schema ({
    name: String,
    firstName: String,
    lastName: String,
    born: Date,
    register: Date,
    notes: [noteSchema],
    slides: [slideSchema]
});

var noteSchema = new Schema ({
    created: Date,
    lastModified: Date,
    private: Boolean,
    content: String
});

var slideSchema = new Schema ({
    created: Date,
    lastModified: Date,
    private: Boolean,
    content: String
});

var User = mongoose.model('User', userSchema);