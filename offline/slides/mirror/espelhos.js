var scene, renderer, camera, controls;
var geometry, material, mesh;
var particle;

var verticalMirrorMesh;

var WIDTH = window.innerWidth;
var HEIGHT = window.innerHeight;

var FOV = 75;
var ASPECT = WIDTH / HEIGHT;
var NEAR = 1;
var FAR = 5000;

init();
walls();
lights();
mirror();
lines();
animate();

function init () {
    
    // Create the scene
    scene = new THREE.Scene();
    
    // Create the renderer and add it to the DOM
    renderer = new THREE.WebGLRenderer({ antialias: true });
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(WIDTH, HEIGHT);
    document.body.appendChild(renderer.domElement);
    
    camera = new THREE.PerspectiveCamera(FOV, ASPECT, NEAR, FAR);
    camera.position.set( 0, 75, 160 );
    //scene.add(camera);
    
    controls = new THREE.OrbitControls(camera, renderer.domElement);

    /*
    geometry = new THREE.BoxGeometry(200, 200, 200, 2, 0, 0);
    material = new THREE.MeshBasicMaterial({
        color: 0xff0000,
        wireframe: true
    });

    mesh = new THREE.Mesh(geometry, material);
    scene.add(mesh);
    */
    
    window.addEventListener('resize', function() {
      WIDTH = window.innerWidth;
      HEIGHT = window.innerHeight;
      renderer.setSize(WIDTH, HEIGHT);
      camera.aspect = WIDTH / HEIGHT;
      camera.updateProjectionMatrix();
    });

}

function walls () {
    var planeGeo = new THREE.PlaneBufferGeometry( 200.1, 100.1 );
    
    var planeTopLeft = new THREE.Mesh( planeGeo, new THREE.MeshPhongMaterial( { color: 0xffffff } ) );
    planeTopLeft.position.z = -50;
	planeTopLeft.position.y = 100;
	planeTopLeft.rotateX( Math.PI / 2 );
	scene.add( planeTopLeft );
    
    var planeTopRight = new THREE.Mesh( planeGeo, new THREE.MeshPhongMaterial( { color: 0xffffff } ) );
    planeTopRight.position.z = 50;
	planeTopRight.position.y = 100;
	planeTopRight.rotateX( Math.PI / 2 );
	scene.add( planeTopRight );
				
	var planeBack = new THREE.Mesh( planeGeo, new THREE.MeshPhongMaterial( { color: 0xffffff } ) );
	planeBack.position.z = -100;
	planeBack.position.y = 50;
	scene.add( planeBack );
				
	var planeFront = new THREE.Mesh( planeGeo, new THREE.MeshPhongMaterial( { color: 0xffffff } ) );
	planeFront.position.z = 100;
	planeFront.position.y = 50;
	planeFront.rotateY( Math.PI );
	scene.add( planeFront );
				
	var planeRight = new THREE.Mesh( planeGeo, new THREE.MeshPhongMaterial( { color: 0xffffff } ) );
	planeRight.position.x = 100;
	planeRight.position.y = 50;
	planeRight.rotateY( - Math.PI / 2 );
	scene.add( planeRight );
				
	var planeLeft = new THREE.Mesh( planeGeo, new THREE.MeshPhongMaterial( { color: 0xffffff } ) );
	planeLeft.position.x = -100;
    planeLeft.position.y = 50;
	planeLeft.rotateY( Math.PI / 2 );
	scene.add( planeLeft );
    
    var planeBottomLeft = new THREE.Mesh( planeGeo, new THREE.MeshPhongMaterial( { color: 0xffffff } ) );
    planeBottomLeft.position.z = -50;
	planeBottomLeft.position.y = 0;
	planeBottomLeft.rotateX( Math.PI / -2 );
	scene.add( planeBottomLeft );

    var planeBottomRight = new THREE.Mesh( planeGeo, new THREE.MeshPhongMaterial( { color: 0xffffff } ) );
    planeBottomRight.position.z = 50;
	planeBottomRight.position.y = 0;
	planeBottomRight.rotateX( Math.PI / -2 );
	scene.add( planeBottomRight );
}

function lights () {
	var mainLight = new THREE.PointLight( 0xcccccc, 1.3, 1000 );
	mainLight.position.y = 60;
	scene.add( mainLight );
}

function mirror () {
    verticalMirror = new THREE.Mirror( renderer, camera, { clipBias: 0.003, textureWidth: WIDTH, textureHeight: HEIGHT, color:0x889999 } );
				
	var verticalMirrorMesh = new THREE.Mesh( new THREE.PlaneBufferGeometry( 60, 60 ), verticalMirror.material );
	verticalMirrorMesh.add( verticalMirror );
	verticalMirrorMesh.position.y = 35;
	verticalMirrorMesh.position.z = -15;
	scene.add( verticalMirrorMesh );
    
	var verticalMirrorBack = new THREE.Mesh( new THREE.PlaneBufferGeometry( 60, 60 ), new THREE.MeshPhongMaterial( { color: 0x000000 } ) );
    verticalMirrorBack.rotateX( Math.PI );
    verticalMirrorBack.position.y = 35;
	verticalMirrorBack.position.z = -16;
	scene.add( verticalMirrorBack );
}

function lines () {
    var material = new THREE.LineBasicMaterial({
        color: 0x0000ff
    z

    var geometry = new THREE.Geometry();
    geometry.vertices.push(
	    new THREE.Vector3( -10, 0, 0 ),
	    new THREE.Vector3( 0, 10, 0 ),
	    new THREE.Vector3( 10, 0, 0 )
    );

    var line = new THREE.Line( geometry, material );
    scene.add( line );
}

function model () {
    var loader = new THREE.ColladaLoader();
    loader.load('');
}

function animate () {
    
    requestAnimationFrame(animate);

    //mesh.rotation.x += 0.01;
    //mesh.rotation.y += 0.02;
    
    verticalMirror.render();
    renderer.render(scene, camera);
    controls.update();
}