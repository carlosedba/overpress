//:::::::::::::::::::::::::::::::::::::::::::::::::
//		 ::::::::  :::::::::   ::::::::   ::::::::
//		:+:    :+: :+:    :+: :+:    :+: :+:    :+:
//		+:+        +:+    +:+ +:+               +:+
//		+#++:++#++ +#++:++#+  +#+            +#++:
//		       +#+ +#+        +#+               +#+
//		#+#    #+# #+#        #+#    #+# #+#    #+#
//		 ########  ###         ########   ########)"
//:::::::::::::::::::::::::::::::::::::::::::::::::

var SPC3JS = (function () {
	'use strict';

	var socket = io(document.location.origin);

	var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition;
	var SpeechGrammarList = SpeechGrammarList || webkitSpeechGrammarList;
	var SpeechRecognitionEvent = SpeechRecognitionEvent || webkitSpeechRecognitionEvent;
	var recognition = new SpeechRecognition();
	var recognizing = false;
	var interim_transcript = '';
	var final_transcript = '';

	// API
	var API = {}

	function webSpeech () {
		recognition.continuous = true;
		recognition.interimResults = true;

		recognition.onstart = function() {
			recognizing = true;
		};

		recognition.onerror = function(event) {
			if (event.error == 'no-speech') {
				console.error('No speech');
				return;
			}
			if (event.error == 'audio-capture') {
				console.error('Microphone not found');
				return;
			}
			if (event.error == 'not-allowed') {
				console.error('Not allowed to access the microphone');
				return;
			}
		};

		recognition.onend = function() {
			recognizing = false;

			if (!final_transcript) {
				console.warn('Por favor, fale novamente');
				return;
			}

			interim_transcript = '';
			final_transcript = '';
		};

		recognition.onresult = function(event) {
			interim_transcript = '';
			final_transcript = '';

			if (typeof(event.results) == 'undefined') {
				recognition.onend = null;
				recognition.stop();
				return;
			}

			for (var i = event.resultIndex; i < event.results.length; ++i) {
				if (event.results[i].isFinal) {
					final_transcript += event.results[i][0].transcript;
				} else {
					interim_transcript += event.results[i][0].transcript;
				}
			}

			if (interim_transcript) {
				console.log('Interim: ' + interim_transcript);
			}
			if (final_transcript) {
				console.log('Final: ' + final_transcript);
			}

			if (final_transcript.length > 0) {
				socket.emit('spc3_cl_command', { id: 'yuhu', command: final_transcript });
			}
		}
	}

	function start (e) {
		if (recognizing) {
			recognition.stop();
			return;
		};

		recognition.lang = 'pt-Br';
		recognition.start();
	}

	function eventListeners () {
		var listenButton = document.querySelector('.listen-button');

		document.addEventListener('click', start);
	}

	function init () {
		webSpeech();
		eventListeners();
	}

	init();

	// Return the API
	return API;

})();