// *************************************************** //
// * Overpress ClientWorks *************************** //
// * Alpha Stage ************************************* //
// * Developed by ************************************ //
// * Carlos Eduardo (carlosed_almeida@Live.com) ****** //
// *************************************************** //

var Overpress = (function () {
	'use strict';

	var doc = document,
        win = window,
        
        // Web Components
		OverpressMain = doc.registerElement('overpress-main', { prototype: Object.create(HTMLElement.prototype) }),
        OverpressCounter = doc.registerElement('overpress-counter', { prototype: Object.create(HTMLElement.prototype) }),
        OverpressSlides = doc.registerElement('overpress-slides', { prototype: Object.create(HTMLElement.prototype) }),
        OverpressSlide = doc.registerElement('overpress-slide', { prototype: Object.create(HTMLDivElement.prototype) }),
        OverpressCustom = doc.registerElement('overpress-custom', { prototype: Object.create(HTMLElement.prototype) }),
        
        // Often used
        main = doc.querySelector('overpress-main'),
        slidesWrapper = doc.querySelector('overpress-slides'),
	    slides = doc.querySelectorAll('overpress-slide'),
	    totalSlides = slides.length,
        syncId = main.dataset.id,
        socketClient,
        
        // Defaults
        config = {
            width: 960,
            height: 700,
            margin: 0.5,
            slideCounter: true,
            sync: true,
            localSync: true,
            syncServerAddress: null,
            manualControl: true,
            keyboard: true,
            touch: false,
            mouseWheel: false,
            overview: false,
            slideShow: false,
            secondsPerSlide: 7
        },
        
        // API
	    API = {
            boot: boot,
            toggleOverview: toggleOverview,
	    	goTo: function (num) {
                goTo(num);
	    	},
	    	next: nextUp,
	    	prev: prevUp,
            config: config,
            events: eventListeners
	    };
    
    //Checks if the number is an Integer or Floating Point
    function isInt (num) {
        if (num % 1 === 0) {
            return true;
        } else if (num % 1 !== 0) {
            return false;
        };
    }
    
    // Extend object a with the properties of object b.
    // If there's a conflict, object b takes precedence.
    // Thanks to Hakim El Hattab (@hakimel)
    function extend (a, b) {
        for( var i in b ) {
            a[i] = b[i];
        };
    }
    
    function setCustomActive () {
        for (var i=0; i<totalSlides; i++) {
            if (customStatus(i+1) === 1) {
                //var slide = slideNumQuery(i);
                var first = slides[i].querySelector('overpress-custom');
                first.dataset.active = 'yes';
            };
        };
    }
    
    function customActive (num) {
        var selector = '[data-active="yes"]',
            slide = slideNumQuery(num),
		    query = slide.querySelector(selector);
		return query;
    }
        
	function slideNumQuery (num) {
		var selector = '[data-num="' + num + '"]',
		    query = doc.querySelector(selector);
		return query;
	}
    
	function buildSlides () {
		for (var x=0; x<totalSlides; x++) {
            if (config.overview) {
                slides[x].classList.remove('presentation');
                slides[x].classList.add('overview');
                slidesWrapper.classList.add('overview');
                slides[x].style.width = config.width + 'px';
                slides[x].style.height = config.height + 'px';
                slides[x].style.transform = 'translateZ(-2500px) translate(' + (x * 105) + '%, 0%)';
            } else if (!config.overview) {
                slidesWrapper.classList.remove('overview');
                slides[x].classList.remove('overview');
                slides[x].classList.add('presentation');
                slides[x].style.width = config.width + 'px';
                slides[x].style.height = config.height + 'px';
                var scale = ((slidesWrapper.offsetHeight - (config.margin * 100)) * 100)/config.height;
                slides[x].style.zoom = scale + '%';
            };
            //slides[x].style.left = (slidesWrapper.offsetWidth - (scale * config.width)/100)/2 + 'px';
            //slides[x].style.top = (slidesWrapper.offsetHeight - (scale * config.height)/100)/2 + 'px';
		};
	}
    
    function buildCounter () {
        if (config.slideCounter) {
            var counters = doc.querySelectorAll('overpress-counter');
            for (var x=0; x<totalSlides; x++) {
                if (counters.length === 0) {
                    var counter = doc.createElement('OVERPRESS-COUNTER');
                    var slideNum = doc.createTextNode((x+1) + '');
                    counter.appendChild(slideNum);
                    slides[x].appendChild(counter);
                    var att = doc.createAttribute('class');
                    if (config.overview) {
                        att.value = 'overview';
                    } else if (!config.overview) {
                        att.value = 'presentation';
                    }
                    counter.setAttributeNode(att);
                } else if (counters.length !== 0) {
                    counters[x].classList.toggle('presentation');
                    counters[x].classList.toggle('overview');
                };
            };
        };
    }
    
    function buildCustom () {
        for (var x=0; x<totalSlides; x++) {
			var y = x + 1;
			slides[x].dataset.num = y;
            var custom = slides[x].querySelectorAll('overpress-custom');
            if (custom != 0) {
                for (var j=0; j<custom.length; j++) {
                    var i = j + 1;
                    var customDataNum = slides[x].dataset.num + "." + i;
                    custom[j].dataset.num = customDataNum; 
                };
            };
        };
    }
    
    function buildAll () {
        buildCustom();
        buildCounter();
        buildSlides();
    }
	
	function clearTimes () {
		for (var x=0; x<totalSlides; x++) {
			//var slide = slideNumQuery(x);
			slides[x].classList.remove('far-prev', 'prev', 'current', 'next', 'far-next');
		};
	}
    
    function clearGeneratedStyles () {
        for (var x=0; x<totalSlides; x++) {
            slides[x].removeAttribute('style');
        };
    }

	function updateHistory (num, dontPush) {
		if (!dontPush) {
			var hash = '#' + num;
			if (window.history.pushState) {
				window.history.pushState(num, 'Slide ' + num, hash);
			} else {
				window.location.replace(hash);
			};
		};
	}

	function updateSlideFromHash () {
		var hash = parseFloat(document.location.hash.substr(1));
		if (hash && isInt(hash)) {
			updateSlides(hash, true);
		} else if (hash && !isInt(hash)){
            var slide = parseInt(hash);
            updateSlides(slide, true);
            updateSlides(hash, true);
        } else {
			updateSlides(1, false);
		};
	}
    
    function customStatus (num, back) {
        if (typeof num === 'number'){
            if (isInt(num)){
                var currentSlide = slideNumQuery(num);
                var customLength = currentSlide.querySelectorAll('overpress-custom').length;
                var firstCustom = num + '.' + 1;
                var lastCustom = num + '.' + customLength;
            } else if (!isInt(num)) {
                var str = num + '';
                var strArr = str.split('.');
                var currentSlide = slideNumQuery(strArr[0]);
                var customLength = currentSlide.querySelectorAll('overpress-custom').length;
                var currentCustom = str;
                var firstCustom = num + '.' + 1;
                var lastCustom = strArr[0] + '.' + customLength;
            };
            if (customLength === 0) {
                return 0;
            };
            if (back) {
                if (customLength > 0 && firstCustom !== currentCustom) {
                    return 1;
                } else if (customLength > 0 && firstCustom === currentCustom) {
                    return 2;
                };
            } else {
                if (customLength > 0 && lastCustom !== currentCustom) {
                    return 1;
                } else if (customLength > 0 && lastCustom === currentCustom) {
                    return 2;
                }
            };
        };
    }
    
    function updateCustom (num) {
        if (typeof num === "number" && !isInt(num)){
            var str = num + '';
            var strArr = str.split('.');
            var currentSlide = slideNumQuery(strArr[0]);
            var customArr = currentSlide.querySelectorAll('overpress-custom');
            for (var i=1; i<=customArr.length; i++) {
                customArr[i-1].dataset.active = 'no';
                switch (i) {
                    case parseFloat(strArr[1]):
                        var currentCustom = slideNumQuery(num);
                        currentCustom.dataset.active = 'yes';
                    break;
                };
            };
        };
    }
    
	function updateSlides (num, push) {
        if (typeof num === "number" && isInt(num)) {
            if (!config.overview) {
                var dontPush = push;
                if (num > 0 && num <= totalSlides) {
                    clearTimes();
                    for (var x=num-2; x<=num+2; x++) {
                        switch (x) {
                            case num - 2:
                                if (num-2>0) {
                                    var slide = slideNumQuery(num-2);
                                    slide.classList.add('far-prev');
                                };
                            break;
                            case num - 1:
                                if (num-1>0) {
                                    var slide = slideNumQuery(num-1);
                                    slide.classList.add('prev');
                                };
                                break;
                            case num:
                                if (num>0) {
                                    var slide = slideNumQuery(num);
                                    slide.classList.add('current');
                                };
                            break;
                            case num + 1:
                                if (num+1<=totalSlides) {
                                    var slide = slideNumQuery(num+1);
                                    slide.classList.add('next');
                                };
                            break;
                            case num + 2:
                                if (num+2<=totalSlides) {
                                    var slide = slideNumQuery(num+2);
                                    slide.classList.add('far-next');
                                };
                            break;
                        };
                    };
                    updateHistory(num, dontPush);
                };
            };
        } else if (typeof num === "number" && !isInt(num) && !config.overview){
            var element = slideNumQuery(num);
            updateCustom(num);
            if (element.dataset.animation && element.dataset.action) {
                console.info('    info - updateSlides - animation: ' + element.dataset.animation + ' - action: ' + element.dataset.action);
            } else if (element.dataset.animation) {
                console.info('    info - updateSlides - animation: ' + element.dataset.animation);
            } else if (element.dataset.action) {
                console.info('    info - updateSlides - action: ' + element.dataset.action);
            }
            updateHistory(num, dontPush);
        };
	}
    
    function goTo (num) {
        if (isInt(num)) {
            updateSlides(num);
        } else if (!isInt(num)) {
            var slide = parseInt(num);
            updateSlides(slide);
            updateSlides(num);
        };
        socketClient.emit('socketUpdateSlides', { id : syncId, slide : num});
    }
    
    function prevUp () {
        var currentSlide = parseInt(document.location.hash.substr(1));
        updateSlides(currentSlide - 1);
        socketClient.emit('socketUpdateSlides', { id : syncId, slide : currentSlide - 1});
	   /*
        var hashNum = parseFloat(document.location.hash.substr(1));
        var prevHashNum = parseFloat(document.location.hash.substr(1) - 1);
        var currentSlide = slideNumQuery(parseInt(hashNum));
        if (customStatus(prevHashNum, 1) === 1) {var activeCustom = parseFloat(customActive().dataset.num);
                    var unfixedPrev = activeCustom - 0.1 + '';
                    var strArr = unfixedPrev.split('.');
                    var twoDigits = strArr[1].substr(0, 2);
                    var firstCustom = parseFloat(strArr[0] + '.' + 1);
                    if (isInt(hashNum)) {
                        var prevCustom = activeCustom;
                        //console.info('API.next - if (customStatus(hashNum) === 1) -> if (isInt(' + parseInt(hashNum) + '))');
                    } else if (!isInt(hashNum) && hashNum !== firstCustom) {
                        var prevCustom = parseFloat(strArr[0] + '.' + twoDigits);
                        //console.log('API.next - if (customStatus(hashNum) === 1) -> if (!isInt(' + hashNum + ') && hashNum: ' + hashNum + ' !== lastCustom: ' + lastCustom + ') - nextCustom: ' + nextCustom);
                    }
                    updateSlides(prevCustom);
                    //console.info('API.next - if (customStatus(hashNum) === 1) - ' + nextCustom);
                } else if (parseInt(hashNum - 1) <= totalSlides && !customStatus(hashNum, 1) || customStatus(hashNum, 1) === 2) {
                    updateSlides(parseInt(hashNum - 1));
                    //console.info('API.next - if (!customStatus(hashNum) || customStatus(hashNum) === 2) - ' + (parseInt(hashNum + 1)) );
                }*/
    }
    
    function nextUp () {
        var hashNum = parseFloat(document.location.hash.substr(1));
        var currentSlide = slideNumQuery(parseInt(hashNum));
        if (customStatus(hashNum) === 1) {
            var activeCustom = parseFloat(customActive(parseInt(hashNum)).dataset.num);
            var unfixedNext = activeCustom + 0.1 + '';
            var strArr = unfixedNext.split('.');
            var twoDigits = strArr[1].substr(0, 2);
            var lastCustom = parseFloat(strArr[0] + '.' + currentSlide.querySelectorAll('overpress-custom').length);
            if (isInt(hashNum)) {
                if (activeCustom === lastCustom) {
                    var nextCustom = parseFloat(strArr[0] + '.' + 1);
                    } else {
                        var nextCustom = activeCustom;
                    }
                //console.info('API.next - if (customStatus(hashNum) === 1) -> if (isInt(' + parseInt(hashNum) + '))');
            } else if (!isInt(hashNum) && hashNum !== lastCustom) {
                var nextCustom = parseFloat(strArr[0] + '.' + twoDigits);
                //console.log('API.next - if (customStatus(hashNum) === 1) -> if (!isInt(' + hashNum + ') && hashNum: ' + hashNum + ' !== lastCustom: ' + lastCustom + ') - nextCustom: ' + nextCustom);
            }
            socketClient.emit('socketUpdateSlides', { id : syncId, slide : nextCustom});
            updateSlides(nextCustom);
            //console.info('API.next - if (customStatus(hashNum) === 1) - ' + nextCustom);
        } else if (parseInt(hashNum + 1) <= totalSlides && !customStatus(hashNum) || customStatus(hashNum) === 2) {
            socketClient.emit('socketUpdateSlides', { id : syncId, slide : parseInt(hashNum + 1)});
            updateSlides(parseInt(hashNum + 1));
            //console.info('API.next - if (!customStatus(hashNum) || customStatus(hashNum) === 2) - ' + (parseInt(hashNum + 1)) );
        }
    }
    
    function toggleOverview () {
        if (config.overview) {
            config.overview = false;
        } else if (!config.overview) {
            config.overview = true;
        }
        main.classList.toggle('overview');
        clearTimes();
        clearGeneratedStyles();
        buildAll(); 
        setCustomActive();
        eventListeners();
        updateSlideFromHash();
    }
    
    function webSockets () {
        if (config.sync) {
            if (config.localSync) {
                socketClient = io(doc.location.origin);
            } else if (!config.localSync && config.syncServerAddress !== null) {
                socketClient = io(config.syncServerAddress);
            } else if (!config.localSync && config.syncServerAddress === null) {
                console.error('Deu treta!');
            }
            socketClient.on('changeSlides', function (data) {
                //console.log(data);
                var hash = parseFloat(document.location.hash.substr(1));
                var slide = data.slide;
                var id = data.id;
                if (slide !== hash && syncId === id) {
                    if (isInt(slide)) {
                        updateSlides(slide);
                    } else if (!isInt(slide)) {
                        var slideNum = parseInt(slide);
                        updateSlides(slideNum);
                        updateSlides(slide);
                    };
                    console.info('    info - socketUpdateSlides - id:' + id + ' - slide:' + slide);
                };
            });
        };
    }

	function eventListeners () {
        /*function overviewGoTo (e) {
            console.log(e.toElement.offsetParent.dataset.num);
            goTo(e.toElement.offsetParent.dataset.num);
        };*/
        
		win.addEventListener('popstate', function (e) {
			if (e.state != null) {
				updateSlides(e.state, true);
				e.preventDefault();
			} else {
				updateSlideFromHash();
			};
		}, false);
        
        win.addEventListener('resize', function (e) {
            buildSlides();
		}, false);
        
        /*for (var x=0; x<totalSlides; x++) {
            if (config.overview) {
                slides[x].addEventListener('click', overviewGoTo, false);
                console.log('sim ' + x);
            } else if (!config.overview) {
                slides[x].removeEventListener('click', overviewGoTo, false);
                console.log('nao' + x);
            };
        };*/
        
        if (config.keyboard) {
            doc.addEventListener('keydown', function (e) {
                switch (e.keyCode) {
                    case 38:
				    case 39:
                        nextUp();
				    break;
				    case 40:
				    case 37:
				        prevUp();
				    break;
                    case 79:
                        toggleOverview();
                    break;
             };
            }, false);
        };
	}
    
    // Initialization sequence
    function boot (options) {
        extend(config, options);
        buildAll();
        setCustomActive();
        webSockets();
        eventListeners();
        updateSlideFromHash();
    }

	// Return the API
	return API;

})();