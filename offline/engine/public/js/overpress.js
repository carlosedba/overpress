// *************************************************** //
// * Overpress ClientWorks *************************** //
// * Alpha Stage ************************************* //
// * Developed by ************************************ //
// * Developed by Carlos Eduardo (@CarlosEBDA) ******* //
// *************************************************** //

var Overpress = (function () {
	'use strict';

	var doc = document,
        win = window,
        
        // Web Components
		OverpressMain = document.registerElement('overpress-main', { prototype: Object.create(HTMLElement.prototype) }),
        OverpressCounter = document.registerElement('overpress-counter', { prototype: Object.create(HTMLElement.prototype) }),
        OverpressSlides = document.registerElement('overpress-slides', { prototype: Object.create(HTMLElement.prototype) }),
        OverpressSlide = document.registerElement('overpress-slide', { prototype: Object.create(HTMLDivElement.prototype) }),
        OverpressCustom = document.registerElement('overpress-custom', { prototype: Object.create(HTMLElement.prototype) }),
        
        // Often used
        main = document.querySelector('overpress-main'),
        slidesWrapper = document.querySelector('overpress-slides'),
	    slides = document.querySelectorAll('overpress-slide'),
	    totalSlides = slides.length,
        viewModes = ['presentation', 'overview', 'devAuthor', 'devPocket', 'devFront'],
        viewStages = ['far-prev', 'prev', 'current', 'next', 'far-next', 'unfocused'],
        syncId = main.dataset.id,
        socketClient,
        counters,
        playPause,
        
        // Defaults
        config = {
            width: 960,
            height: 700,
            margin: 0.5,
            slideCounter: true,
            sync: true,
            localSync: true,
            syncServerAddress: null,
            manualControl: true,
            embedded: false,
            keyboard: true,
            touch: true,
            mouseWheel: false,
            slideShow: false,
            secondsPerSlide: 7,
            videoController: false,
            videoReceptor: false,
            videoStatus: 'paused',
            viewMode: viewModes[0] // 'presentation' [0], 'overview [1]', 'devAuthor [2]', 'devPocket [3]', 'devFront [4]'
        },
        
        // API
	    API = {
            boot: boot,
	    	goTo: function (num) {
                goTo(num);
	    	},
	    	next: nextUp,
	    	prev: prevUp,
            config: config,
            events: eventListeners,
            buildCounter: buildCounter,
            buildSlides: buildSlides,
            clearGeneratedStyles: function (option) {
                clearGeneratedStyles(option);
            },
            counters: counters,
            socket: getSocket
	    },
        
        // Touch
        touch = {
			startX: 0,
			startY: 0,
			startSpan: 0,
			startCount: 0,
			captured: false,
			threshold: 40
		};
    
    //Checks if the number is an Integer or Floating Point
    function isInt (num) {
        if (num % 1 === 0) {
            return true;
        } else if (num % 1 !== 0) {
            return false;
        }
    }
    
    // Extend object a with the properties of object b.
    // If there's a conflict, object b takes precedence.
    // Thanks to Hakim El Hattab (@hakimel)
    function extend (a, b) {
        for( var i in b ) {
            a[i] = b[i];
        }
    }

    function getSocket () {
        return socketClient;
    }

    function setVideoController () {
        if (config.videoController) {
            var video = document.querySelectorAll('video');
            playPause = document.querySelectorAll('.play-pause');

            function play (e) {
                var el = e.target.parentElement.querySelector('.play-pause');
                togglePlayPause(0, el);
            }

            function pause (e) {
                var el = e.target.parentElement.querySelector('.play-pause');
                togglePlayPause(1, el);
            }

            [].forEach.call(playPause, function (el, ind, arr) {
                el.addEventListener('click', function (e) {
                    if (config.videoStatus == 'paused') {
                        config.videoStatus = 'playing';
                        socketClient.emit('socketVideoController', { status: 'play', index: ind });
                        video[ind].play();
                    } else if (config.videoStatus == 'playing') {
                        config.videoStatus = 'paused';
                        socketClient.emit('socketVideoController', { status: 'pause', index: ind });
                        video[ind].pause();
                    }
                });
            });

            [].forEach.call(video, function (el, ind, arr) {
                el.addEventListener('play', play);
                el.addEventListener('pause', pause);
            });
        }
    }

    function setVideoReceptor () {
        if (config.videoReceptor) {
            var video = document.querySelectorAll('video');
            var controls = document.querySelectorAll('.video-controls');

            [].forEach.call(controls, function (el, ind, arr) {
                el.style.display = 'none';
            });

            socketClient.on('socketVideoSignal', function (data) {
                console.log(data);
                if (data.status == 'play') {
                    video[data.index].play();
                } else if (data.status == 'pause') {
                    video[data.index].pause();
                }
            });
        }
    }

    function togglePlayPause (play, el) {
         if (play) {
            el.classList.remove('pause');
            el.classList.add('play');
        } else {
            el.classList.remove('play');
            el.classList.add('pause');
        }
    }
    
	function slideNumQuery (num) {
		var selector = '[data-num="' + num + '"]',
		    query = document.querySelector(selector);
		return query;
	} 
    
    function clearViewModes (option) {
        for (var x=0; x<totalSlides; x++) {
            for (var y=0; y<viewModes.length; y++) {
                if (option == 'slide') {
                    slidesWrapper.classList.remove(viewModes[y]);
                    slides[x].classList.remove(viewModes[y]);
                } else if (option == 'counter') {
                    counters[x].classList.remove(viewModes[y]);
                } else if (option === null) {
                    slidesWrapper.classList.remove(viewModes[y]);
                    slides[x].classList.remove(viewModes[y]);
                    counters[x].classList.remove(viewModes[y]);
                }
            }
        }
    }
    
	function clearViewStages () {
        for (var x=0; x<totalSlides; x++) {
            for (var y=0; y<viewStages.length; y++) {
                slides[x].classList.remove(viewStages[y]);
            }
        }
    }
    
    function clearGeneratedStyles () {
        for (var x=0; x<totalSlides; x++) {
            slides[x].removeAttribute('style');
        }
    }
    
    function customStatus (num, back) {
        if (typeof num === 'number'){
            if (isInt(num)){
                var currentSlide = slideNumQuery(num);
                var customLength = currentSlide.querySelectorAll('overpress-custom').length;
                var firstCustom = num + '.' + 1;
                var lastCustom = num + '.' + customLength;
            } else if (!isInt(num)) {
                var str = num + '';
                var strArr = str.split('.');
                var currentSlide = slideNumQuery(strArr[0]);
                var customLength = currentSlide.querySelectorAll('overpress-custom').length;
                var currentCustom = str;
                var firstCustom = num + '.' + 1;
                var lastCustom = strArr[0] + '.' + customLength;
            };
            if (customLength === 0) {
                return 0;
            };
            if (back) {
                if (customLength > 0 && firstCustom !== currentCustom) {
                    return 1;
                } else if (customLength > 0 && firstCustom === currentCustom) {
                    return 2;
                };
            } else {
                if (customLength > 0 && lastCustom !== currentCustom) {
                    return 1;
                } else if (customLength > 0 && lastCustom === currentCustom) {
                    return 2;
                }
            };
        };
    }
    
    function getCustomActive (num) {
        var selector = '[data-active="yes"]',
            slide = slideNumQuery(num),
		    query = slide.querySelector(selector);
		return query;
    }
    
    function setCustomActive () {
        for (var i=0; i<totalSlides; i++) {
            if (customStatus(i+1) === 1) {
                var first = slides[i].querySelector('overpress-custom');
                first.dataset.active = 'yes';
            };
        };
    }
    
    function buildSlides () {
        if (config.viewMode) {
            clearViewModes('slide');
            clearViewStages();
            clearGeneratedStyles();
            for (var x=0; x<totalSlides; x++) {
                
                // config.viewMode = 'presentation'
                if (config.viewMode === viewModes[0]) {
                    slidesWrapper.classList.add(viewModes[0]);
                    slides[x].classList.add(viewModes[0]);
                    slides[x].style.width = config.width + 'px';
                    slides[x].style.height = config.height + 'px';
                    var scale = ((slidesWrapper.offsetHeight - (config.margin * 100)) * 100)/config.height;
                    slides[x].style.zoom = scale + '%';
                }
                
                // config.viewMode = 'overview'
                else if (config.viewMode === viewModes[1]) {
                    slidesWrapper.classList.add(viewModes[1]);
                    slides[x].classList.add(viewModes[1]);
                    slides[x].style.width = config.width + 'px';
                    slides[x].style.height = config.height + 'px';
                }
                
                // config.viewMode = 'devAuthor'
                else if (config.viewMode === viewModes[2]) {
                    main.style.width = '585px';
                    main.style.height = '430px';
                    slidesWrapper.classList.add(viewModes[2]);
                    slides[x].classList.add(viewModes[2]);
                    slides[x].style.width = config.width + 'px';
                    slides[x].style.height = config.height + 'px';
                    var scale = ((slidesWrapper.offsetHeight) * 99)/config.height;
                    slides[x].style.zoom = scale + '%';
                }
                
                // config.viewMode = 'devFront'
                else if (config.viewMode === viewModes[4]) {
                    slidesWrapper.classList.add(viewModes[4]);
                    slidesWrapper.style.width = config.width + 'px';
                    slidesWrapper.style.height = config.height + 'px';
                    slides[x].classList.add(viewModes[4]);
                    slides[x].style.width = config.width + 'px';
                    slides[x].style.height = config.height + 'px';
                }
            }
        } else {
            console.error('Deu TRETA!');
        }
    }
    
    function buildCustom () {
        for (var x=0; x<totalSlides; x++) {
			var y = x + 1;
			slides[x].dataset.num = y;
            var custom = slides[x].querySelectorAll('overpress-custom');
            if (custom != 0) {
                for (var j=0; j<custom.length; j++) {
                    var i = j + 1;
                    var customDataNum = slides[x].dataset.num + "." + i;
                    custom[j].dataset.num = customDataNum; 
                };
            };
        };
    }
    
    function buildCounter () {
        if (config.slideCounter) {
            counters = document.querySelectorAll('overpress-counter');
            if (counters.length === 0) {
                for (var x=0; x<totalSlides; x++) {
                    var counter = document.createElement('overpress-counter');
                    var slideNum = document.createTextNode((x+1) + '');
                    counter.appendChild(slideNum);
                    slides[x].appendChild(counter);
                    var att = document.createAttribute('class');
                    att.value = config.viewMode;
                    counter.setAttributeNode(att);
                }
            } else {
                clearViewModes('counter');
                for (var x=0; x<totalSlides; x++) {
                    counters[x].classList.add(config.viewMode);
                }
            }
        }
    }
    
    function buildAuthorInfo () {
        if (config.viewMode === 'devAuthor') {
            var OverpressMask = document.registerElement('overpress-mask', { prototype: Object.create(HTMLElement.prototype) });
            
            var maskTop = document.createElement('overpress-mask');
            maskTop.classList.add(config.viewMode);
            main.appendChild(maskTop);
                        
            var OverpressAuthorProto = Object.create(HTMLElement.prototype);
            OverpressAuthorProto.createdCallback = function () {
                var shadow = this.createShadowRoot();
            
                var picture = document.createElement('img');
                picture.src = this.dataset.picture;
                picture.alt = this.dataset.name;
                shadow.appendChild(picture);
            
                var link = document.createElement('a');
                link.href = this.dataset.profile;
                link.innerHTML = this.dataset.name;
                shadow.appendChild(link);
            
                var date = document.createElement('span');
                date.innerHTML = this.dataset.date;
                shadow.appendChild(date);
            }
            document.registerElement('overpress-author', { prototype: OverpressAuthorProto });
        }
    }
    
    function buildSocialButtons () {
        if (config.viewMode === 'devAuthor' || 'devPocket') {
            var OverpressSocial = document.registerElement('overpress-social', { prototype: Object.create(HTMLElement.prototype) });
            
            var socialWrapper = document.createElement('overpress-social');
            socialWrapper.dataset.viewMode = config.viewMode;
            main.appendChild(socialWrapper);
            
            var socialButtonProto = Object.create(HTMLElement.prototype);
            socialButtonProto.createdCallback = function () {
                var shadow = this.createShadowRoot();
                var icon = document.createElement('img');
                icon.src = this.dataset.icon;
                shadow.appendChild(icon);
            }
            document.registerElement('social-button', { prototype: socialButtonProto });          
        }
    }
    
    function buildAll () {
        buildSlides();
        buildCustom();
        buildCounter();
        buildAuthorInfo();
        //buildSocialButtons();
    }
	
	function updateHistory (num, dontPush) {
		if (!dontPush) {
			var hash = '#' + num;
			if (window.history.pushState) {
				window.history.pushState(num, 'Slide ' + num, hash);
			} else {
				window.location.replace(hash);
			};
		};
	}

	function updateSlideFromHash () {
		var hash = parseFloat(document.location.hash.substr(1));
		if (hash && isInt(hash)) {
			updateSlides(hash, true);
		} else if (hash && !isInt(hash)){
            var slide = parseInt(hash);
            updateSlides(slide, true);
            updateSlides(hash, true);
        } else {
			updateSlides(1, false);
		};
	}
    
    function updateCustom (num) {
        if (typeof num === "number" && !isInt(num)){
            var str = num + '';
            var strArr = str.split('.');
            var currentSlide = slideNumQuery(strArr[0]);
            var customArr = currentSlide.querySelectorAll('overpress-custom');
            for (var i=1; i<=customArr.length; i++) {
                customArr[i-1].dataset.active = 'no';
                switch (i) {
                    case parseFloat(strArr[1]):
                        var currentCustom = slideNumQuery(num);
                        currentCustom.dataset.active = 'yes';
                    break;
                };
            };
        };
    }
    
	function updateSlides (num, push) {
        if (typeof num === "number" && isInt(num)) {
                var dontPush = push;
                if (num > 0 && num <= totalSlides) {
                    clearViewStages();
                    
                    if (config.viewMode === 'presentation' || config.viewMode === 'devAuthor' || config.viewMode === 'devPocket' || config.viewMode === 'devFront') {
                        for (var i = num-2; i>0; i--) {
                            var slide = slideNumQuery(i);
                            slide.classList.add('far-prev');
                        }
                        for (var i=num-1; i<=num+1; i++) {
                            switch (i) {
                                case num - 1:
                                    if (num-1>0) {
                                        var slide = slideNumQuery(num-1);
                                        slide.classList.add('prev');
                                    }
                                    break;
                                case num:
                                    if (num>0) {
                                        var slide = slideNumQuery(num);
                                        slide.classList.add('current');
                                    }
                                    break;
                                case num + 1:
                                    if (num+1<=totalSlides) {
                                        var slide = slideNumQuery(num+1);
                                        slide.classList.add('next');
                                    }
                                    break;
                            }
                        }
                        for (var i = num+2; i<=totalSlides; i++) {
                            var slide = slideNumQuery(i);
                            slide.classList.add('far-next');
                        }
                    } else if (config.viewMode === 'overview') {
                        var x = 105;
                        for (var i = num; i>0; i--) {
                            var slide = slideNumQuery(i);
                            slide.style.transform = 'translateZ(-2500px) translate(' + (x-=105) + '%, 0%)';
                            if (i !== num) {
                                slide.classList.add('unfocused');
                            }
                        }
                        var x = -105;
                        for (var i=num; i<=totalSlides; i++) {
                            var slide = slideNumQuery(i);
                            slide.style.transform = 'translateZ(-2500px) translate(' + (x+=105) + '%, 0%)';
                            if (i !== num) {
                                slide.classList.add('unfocused');
                            }
                        }
                    }
                    updateHistory(num, dontPush);
                };
        } else if (typeof num === "number" && !isInt(num)){
            var element = slideNumQuery(num);
            updateCustom(num);
            if (element.dataset.animation && element.dataset.action) {
                console.info('    info - updateSlides - animation: ' + element.dataset.animation + ' - action: ' + element.dataset.action);
            } else if (element.dataset.animation) {
                console.info('    info - updateSlides - animation: ' + element.dataset.animation);
            } else if (element.dataset.action) {
                console.info('    info - updateSlides - action: ' + element.dataset.action);
            }
            updateHistory(num, dontPush);
        };
	}
    
    function goTo (num) {
        if (isInt(num)) {
            updateSlides(num);
        } else if (!isInt(num)) {
            var slide = parseInt(num);
            updateSlides(slide);
            updateSlides(num);
        };
        socketClient.emit('socketUpdateSlides', { id : syncId, slide : num});
    }
    
    function prevUp () {
        var currentSlide = parseInt(document.location.hash.substr(1));
        updateSlides(currentSlide - 1);
        socketClient.emit('socketUpdateSlides', { id : syncId, slide : currentSlide - 1});
	   /*
        var hashNum = parseFloat(document.location.hash.substr(1));
        var prevHashNum = parseFloat(document.location.hash.substr(1) - 1);
        var currentSlide = slideNumQuery(parseInt(hashNum));
        if (customStatus(prevHashNum, 1) === 1) {var activeCustom = parseFloat(getCustomActive().dataset.num);
                    var unfixedPrev = activeCustom - 0.1 + '';
                    var strArr = unfixedPrev.split('.');
                    var twoDigits = strArr[1].substr(0, 2);
                    var firstCustom = parseFloat(strArr[0] + '.' + 1);
                    if (isInt(hashNum)) {
                        var prevCustom = activeCustom;
                        //console.info('API.next - if (customStatus(hashNum) === 1) -> if (isInt(' + parseInt(hashNum) + '))');
                    } else if (!isInt(hashNum) && hashNum !== firstCustom) {
                        var prevCustom = parseFloat(strArr[0] + '.' + twoDigits);
                        //console.log('API.next - if (customStatus(hashNum) === 1) -> if (!isInt(' + hashNum + ') && hashNum: ' + hashNum + ' !== lastCustom: ' + lastCustom + ') - nextCustom: ' + nextCustom);
                    }
                    updateSlides(prevCustom);
                    //console.info('API.next - if (customStatus(hashNum) === 1) - ' + nextCustom);
                } else if (parseInt(hashNum - 1) <= totalSlides && !customStatus(hashNum, 1) || customStatus(hashNum, 1) === 2) {
                    updateSlides(parseInt(hashNum - 1));
                    //console.info('API.next - if (!customStatus(hashNum) || customStatus(hashNum) === 2) - ' + (parseInt(hashNum + 1)) );
                }*/
    }
    
    function nextUp () {
        var hashNum = parseFloat(document.location.hash.substr(1));
        var currentSlide = slideNumQuery(parseInt(hashNum));
        if (customStatus(hashNum) === 1) {
            var activeCustom = parseFloat(getCustomActive(parseInt(hashNum)).dataset.num);
            var unfixedNext = activeCustom + 0.1 + '';
            var strArr = unfixedNext.split('.');
            var twoDigits = strArr[1].substr(0, 2);
            var lastCustom = parseFloat(strArr[0] + '.' + currentSlide.querySelectorAll('overpress-custom').length);
            if (isInt(hashNum)) {
                if (activeCustom === lastCustom) {
                    var nextCustom = parseFloat(strArr[0] + '.' + 1);
                    } else {
                        var nextCustom = activeCustom;
                    }
                //console.info('API.next - if (customStatus(hashNum) === 1) -> if (isInt(' + parseInt(hashNum) + '))');
            } else if (!isInt(hashNum) && hashNum !== lastCustom) {
                var nextCustom = parseFloat(strArr[0] + '.' + twoDigits);
                //console.log('API.next - if (customStatus(hashNum) === 1) -> if (!isInt(' + hashNum + ') && hashNum: ' + hashNum + ' !== lastCustom: ' + lastCustom + ') - nextCustom: ' + nextCustom);
            }
            socketClient.emit('socketUpdateSlides', { id : syncId, slide : nextCustom});
            updateSlides(nextCustom);
            //console.info('API.next - if (customStatus(hashNum) === 1) - ' + nextCustom);
        } else if (parseInt(hashNum + 1) <= totalSlides && !customStatus(hashNum) || customStatus(hashNum) === 2) {
            socketClient.emit('socketUpdateSlides', { id : syncId, slide : parseInt(hashNum + 1)});
            updateSlides(parseInt(hashNum + 1));
            //console.info('API.next - if (!customStatus(hashNum) || customStatus(hashNum) === 2) - ' + (parseInt(hashNum + 1)) );
        }
    }
    
    function webSockets () {
        if (config.sync) {
            if (config.localSync) {
                socketClient = io(document.location.origin);
            } else if (!config.localSync && config.syncServerAddress !== null) {
                socketClient = io(config.syncServerAddress);
            } else if (!config.localSync && config.syncServerAddress === null) {
                console.error('Deu treta!');
            }
            socketClient.on('changeSlides', function (data) {
                //console.log(data);
                var hash = parseFloat(document.location.hash.substr(1));
                var slide = data.slide;
                var id = data.id;
                if (slide !== hash && syncId === id) {
                    if (isInt(slide)) {
                        updateSlides(slide);
                    } else if (!isInt(slide)) {
                        var slideNum = parseInt(slide);
                        updateSlides(slideNum);
                        updateSlides(slide);
                    };
                    console.info('    info - socketUpdateSlides - id:' + id + ' - slide:' + slide);
                };
            });
        };
    }
    
    	/**
	 * Handler for the 'touchstart' event, enables support for
	 * swipe and pinch gestures.
	 */
	function onTouchStart(event) {

		touch.startX = event.touches[0].clientX;
		touch.startY = event.touches[0].clientY;
		touch.startCount = event.touches.length;

	}

	/**
	 * Handler for the 'touchmove' event.
	 */
	function onTouchMove( event ) {

		// Each touch should only trigger one action
		if( !touch.captured ) {
			//onUserInput( event );

			var currentX = event.touches[0].clientX;
			var currentY = event.touches[0].clientY;

			// If the touch started with two points and still has
			// two active touches; test for the pinch gesture
            /*
			if( event.touches.length === 2 && touch.startCount === 2 && config.overview ) {

				// The current distance in pixels between the two touch points
				var currentSpan = distanceBetween( {
					x: event.touches[1].clientX,
					y: event.touches[1].clientY
				}, {
					x: touch.startX,
					y: touch.startY
				} );

				// If the span is larger than the desire amount we've got
				// ourselves a pinch
				if( Math.abs( touch.startSpan - currentSpan ) > touch.threshold ) {
					touch.captured = true;

					if( currentSpan < touch.startSpan ) {
						activateOverview();
					}
					else {
						deactivateOverview();
					}
				}

				event.preventDefault();

			}
			// There was only one touch point, look for a swipe
			else */if( event.touches.length === 1 && touch.startCount !== 2 ) {

				var deltaX = currentX - touch.startX,
					deltaY = currentY - touch.startY;

				if( deltaX > touch.threshold && Math.abs( deltaX ) > Math.abs( deltaY ) ) {
					touch.captured = true;
					prevUp();
				}
				else if( deltaX < -touch.threshold && Math.abs( deltaX ) > Math.abs( deltaY ) ) {
					touch.captured = true;
					nextUp();
				}
				/*else if( deltaY > touch.threshold ) {
					touch.captured = true;
					navigateUp();
				}
				else if( deltaY < -touch.threshold ) {
					touch.captured = true;
					navigateDown();
				}*/

				// If we're embedded, only block touch events if they have
				// triggered an action
				if( config.embedded ) {
					if( touch.captured || isVerticalSlide( currentSlide ) ) {
						event.preventDefault();
					}
				}
				// Not embedded? Block them all to avoid needless tossing
				// around of the viewport in iOS
				else {
					event.preventDefault();
				}

			}
		}
		// There's a bug with swiping on some Android devices unless
		// the default action is always prevented
		else if( navigator.userAgent.match( /android/gi ) ) {
			event.preventDefault();
		}

	}

	/**
	 * Handler for the 'touchend' event.
	 */
	function onTouchEnd(event) {

		touch.captured = false;

	}

	function eventListeners () {
        /*function overviewGoTo (e) {
            console.log(e.toElement.offsetParent.dataset.num);
            goTo(e.toElement.offsetParent.dataset.num);
        };*/
        
		win.addEventListener('popstate', function (e) {
			if (e.state != null) {
				updateSlides(e.state, true);
				e.preventDefault();
			} else {
				updateSlideFromHash();
			};
		}, false);
        
        win.addEventListener('resize', function (e) {
            buildSlides();
            updateSlideFromHash();
		}, false);
        
        /*for (var x=0; x<totalSlides; x++) {
            if (config.overview) {
                slides[x].addEventListener('click', overviewGoTo, false);
                console.log('sim ' + x);
            } else if (!config.overview) {
                slides[x].removeEventListener('click', overviewGoTo, false);
                console.log('nao' + x);
            };
        };*/
        
        if (config.keyboard) {
            document.addEventListener('keydown', function (e) {
                switch (e.keyCode) {
                    case 38:
				    case 39:
                        nextUp();
				    break;
				    case 40:
				    case 37:
				        prevUp();
				    break;
                    /*
                    case 79:
                        toggleOverview();
                    break;
                    */
             };
            }, false);
        }
        
        if (config.touch) {
            document.addEventListener('touchstart', onTouchStart, false);
            document.addEventListener('touchmove', onTouchMove, false);
            document.addEventListener('touchend', onTouchEnd, false);
        }
	}
    
    // Initialization sequence
    function boot (options) {
        extend(config, options);
        buildAll();
        setCustomActive();
        webSockets();
        eventListeners();
        updateSlideFromHash();
        setVideoController();
        setVideoReceptor();
    }

	// Return the API
	return API;

})();