/// <reference path="typings/node/node.d.ts"/>
// Set the require wrapper
global.rootRequire = function(name) {
    return require(__dirname + '/' + name);
}

global.publicDir = __dirname + '/public';
global.slidesDir = '../slides';

// Defaults
var MONGO_URI = 'mongodb://np:}Bps2xXUW@127.0.0.1:27025/np';
var MONGOLAB_URI = process.env.MONGOLAB_URI;

// Default Modules
var express = require('express');
var readline = require('linebyline');
var helmet = require('helmet');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var multer = require('multer');
var passport = require('passport');
var mongoose = require('mongoose');
var program = require('commander');
var io = require('socket.io');

// Root Modules
var http = rootRequire('patchs/http');
var P2C = rootRequire('Print2Console');

// Default Variables
//var rl = readline.createInterface(process.stdin, process.stdout);
var app = express();
var router = express.Router();
var Schema = mongoose.Schema;

program
    .version('0.1.3')
    .description('Overpress Network Platform | Server')
    .option('-p, --port [port]', 'port to listen for', 4400)
    .option('-m, --mode [mode]', 'what mode the server should run [prod|dev]', 'dev')
    .parse(process.argv);

if (program.mode === 'dev') {
    P2C.dev();
} else if (program.mode === 'prod') {
    P2C.prod();
}

// Set the port
var PORT = program.port;

/*
//
// HTTP Server
//
*/
app.use(
    express.static(publicDir),
    bodyParser.json(),
    bodyParser.urlencoded({ extended: true }),
    cookieParser('menes'),
    session({
        secret: 'menes',
        cookie: {
            path: '/',
            httpOnly: true,
            secure: false,
            maxAge: null
        },
        rolling: false,
        resave: false,
        proxy: undefined,
        saveUninitialized: false,
        unset: 'keep'
    }),
    multer(),
    helmet.xssFilter(),
    helmet.frameguard('deny'),
    helmet.hidePoweredBy({ setTo: 'OverpressNP' }),
    helmet.noSniff(),
    //helmet.crossdomain(),
    passport.initialize(),
    passport.session()
);

// Apply patchs to the middleware
http.badRequest(app);

// Setup the routes
rootRequire('routes/logger')(app);
rootRequire('routes/index')(express, app);
rootRequire('routes/slide')(express, app);
//rootRequire('routes/public')(express, app); // Desnecessauro

// BootUp the HTTP Server
var server = app.listen(PORT, function () {
    P2C.info('express', 'Server listening on port ' + PORT);
});

/*
//
// Socket.IO Server
//
*/

io = io(server, { log: false });

var slideHistory = [];
var slideHistoryLen = 0;
var tempSlide = 0;

io.on('connection', function (server) {
    P2C.info('socket.io', 'Client connected!')

    server.on('socketUpdateSlides', function (data) {
        if (data.id && data.slide) {

            if (slideHistory.length) {
                slideHistoryLen = slideHistory.length;
                for (var i = 0; i < slideHistoryLen; i++) {
                    if (slideHistory[i].id === data.id) {
                        tempSlide = slideHistory[i];
                        console.log('tempSlide: ' + tempSlide);
                        break;
                    } else {
                        tempSlide = 0;
                    }
                }

                if (tempSlide) {
                    tempSlide.slide = data.slide;
                    console.log(tempSlide);
                    console.log('Updating Slide Number');
                } else {
                    slideHistory.push({
                        id: data.id,
                        slide: data.slide
                    });
                    console.log('Adding new Slide');
                }
            } else {
                slideHistory.push({
                    id: data.id,
                    slide: data.slide
                });
                console.log('hu3');
            }


            console.log(slideHistory);

            server.broadcast.emit('changeSlides', data);
            P2C.info('socket.io', 'id: ' + data.id + ' - slide: ' + data.slide);
        }
    });

    server.on('socketVideoController', function (data) {
        server.broadcast.emit('socketVideoSignal', data);
        P2C.info('socket.io', 'status: ' + data.status + ' - index: ' + data.index);
    });

    server.on('spc3_cl_command', function (data) {
        if (data.id) {
            //console.log(data);
            server.emit('spc3_node_command', data);
            P2C.info('socket.io', 'id: ' + data.id + ' command: ' + data.command);

            var payload = {};

            if (data.command.search("próxima tela") || data.command.search("próximo slide")) {
                slideHistory.forEach(function (el, ind, arr) {
                    if (el.id === data.id) {
                        payload = {
                            id: el.id,
                            slide: el.slide + 1
                        };
                        el.slide = payload.slide;
                        console.log('++');
                    }
                });
            }

            /*if (data.command.search("tela anterior") || data.command.search("slide anterior")) {
                slideHistory.forEach(function (el, ind, arr) {
                    if (el.id === data.id) {
                        payload = {
                            id: el.id,
                            slide: el.slide - 1
                        };
                        el.slide = payload.slide;
                        console.log('--');
                    }
                });
            }*/

            /*if (data.command.search("ir para slide")) {
                var slide = data.command.search("ir para slide");
                if (data.command.charAt(slide + 14) !== (' ' || '')) {
                    if (data.command.charAt(slide + 15) !== (' ' || '')) {
                        var n1 = data.command.charAt(slide + 14);
                        var n2 = data.command.charAt(slide + 15);

                        slideHistory.forEach(function (el, ind, arr) {
                            if (el.id === data.id) {
                                payload = {
                                    id: el.id,
                                    slide: parseInt(n1 + n2)
                                };
                                el.slide = payload.slide;
                                console.log(n1 + n2);
                            }
                        });
                    } else {
                        slideHistory.forEach(function (el, ind, arr) {
                            if (el.id === data.id) {
                                payload = {
                                    id: el.id,
                                    slide: parseInt(n1)
                                };
                                el.slide = payload.slide;
                                console.log(n1);
                            }
                        });
                    }
                }
            }*/

            server.broadcast.emit('changeSlides', payload);
            P2C.info('socket.io', 'id: ' + payload.id + ' - slide: ' + payload.slide);
            console.log('slide found!');
        }
    });

    server.on('spc3_response', function (data) {
        if (data.response) {
            //console.log(data);
            server.broadcast.emit('spc3_node_response', data);
            P2C.info('socket.io', 'response: ' + data.response);
        }
    });

    server.on('wa_start', function (data) {
        console.log(data);
            server.broadcast.emit('wa_sv_start', data);
            P2C.info('socket.io', 'response: ' + data);
    });

    server.on('wa_stop', function (data) {

            server.broadcast.emit('wa_sv_stop', data);
            P2C.info('socket.io', 'response: ' + data);
        
    });

    server.on('wa_amplitude', function (data) {

            server.broadcast.emit('wa_sv_amplitude', data);
            P2C.info('socket.io', 'response: ' + data);
        
    });

    server.on('wa_frequency', function (data) {

            server.broadcast.emit('wa_sv_frequency', data);
            P2C.info('socket.io', 'response: ' + data);
        
    });

    server.on('wa_speed', function (data) {

            server.broadcast.emit('wa_sv_speed', data);
            P2C.info('socket.io', 'response: ' + data);
        
    });

});

/*
//
// Initialize the Google APIs
//
*/

/*
// Gmail API
rootRequire('GmailAPI/auth')(function () {
    P2C.info('googleapi', 'Access Granted!');
});
*/

/*
//
// Setup the MongoDB Server
//
*/

mongoose.connect(MONGO_URI);
var db = mongoose.connection;

db.on('error', function (err) {
    P2C.error('mongodb', err);
});

db.once('connected', function (callback) {
    P2C.info('mongodb', 'Connected to the Server!');
});

db.on('reconnected', function (callback) {
    P2C.info('mongodb', 'Reconnected to the Server!');
});

db.on('disconnected', function (callback) {
    P2C.warn('mongodb', 'Disconnected from the Server!');
    //mongoose.connect(MONGO_URI);
});