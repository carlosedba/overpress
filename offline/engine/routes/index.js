module.exports = function (express, app) {

    var router = express.Router();

    app.use('/', router);

    router.get('/', function (req, res){
        res.sendFile('index.html', { root : publicDir });
    });

}