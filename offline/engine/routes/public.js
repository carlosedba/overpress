var getPublic = rootRequire('methods/getPublic');

module.exports = function (express, app) {
    var router = express.Router();
    app.use('/public', router);
    
    router.get(/^(.+)$/, getPublic);
}