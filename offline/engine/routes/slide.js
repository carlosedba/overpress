var getLocalSlide = rootRequire('methods/getLocalSlide');

module.exports = function (express, app) {
    var router = express.Router();
    app.use('/slide', router);
    
    router.get('/:name', getLocalSlide.getByName);
    router.get(/^(.+)$/, getLocalSlide.getStatic);
}