var getPublic = function (req, res) {
    res.sendFile(req._parsedUrl.href, { root: publicDir });
}

module.exports = getPublic;