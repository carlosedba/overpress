var P2C = rootRequire('Print2Console');
var fs = require('fs');

var getLocalSlide = {
	
	getByName: function (req, res) {
		fs.readdir(slidesDir, function (err, files) {
			if (err) {
				P2C.error('express', err);
			} else {
				files.forEach(function (name) {
					if (req.params.name === name) {
						res.sendFile('view.html', { root: slidesDir + '/' + name });
						P2C.info('express', name);
					}
				});
			}
		});
	},
	
	getStatic: function (req, res) {
		//console.log(req);
		res.sendFile(req._parsedUrl.href, { root: slidesDir });
	}
	
}

module.exports = getLocalSlide;