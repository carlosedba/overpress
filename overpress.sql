CREATE DATABASE IF NOT EXISTS  overpress CHARACTER SET utf8 COLLATE utf8_general_ci;
USE overpress;
CREATE TABLE IF NOT EXISTS users (
	id int,
	name char(50),
	firstName char(25),
	lastName char(25),
	username char(20),
	born date,
	register date
);
CREATE TABLE IF NOT EXISTS notes (
	id int,
	creator int,
	created datetime,
	lastModified datetime,
	private boolean,
	title char(50),
	content text
);
CREATE TABLE IF NOT EXISTS slides (
	id int,
	creator int,
	created datetime,
	lastModified datetime,
	private boolean,
	title char(50),
	content text
);

INSERT INTO users (id, name, firstName, lastName, username, born, register)
VALUES (1, 'Carlos Eduardo', 'Carlos', 'Eduardo', 'CarlosEDBA', '1999-01-15', '2014-07-10');