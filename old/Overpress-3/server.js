// *************************************************** //
// * Overpress Server Core *************************** //
// * Alpha Stage ************************************* //
// * Developed by: *********************************** //
// * Carlos Eduardo (carlosed_almeida@live.com) ****** //
// *************************************************** //

//
// Set-up initial variables
//

var express = require('express'),
    app = express(),
    router = express.Router(),
    io = require("socket.io"),
    fs = require('fs'),
    path = require('path'),
    port = 1337;

//
// Individual Slide Definitions
//

var home = "../";
var slide = home + "labs";

//
// Routes
//

app.use('/', router);

router.use(function (req, res, next) {
    console.log(req.method, req.url);
    next();
});

router.get('/', function (req, res) {
    res.sendFile('view.html', { root : slide });
});

router.get('/remote', function (req, res) {
    res.sendFile('remote.html', { root : slide });
});

router.get('/external/:filename', function (req, res) {
    res.sendFile('external/' + req.params.filename, { root : slide });
});

router.get('/bower_components/*', function (req, res) {
    res.sendFile('bower_components/' + req.params[0], { root : __dirname });
});

router.get('/overpress_components/*', function (req, res) {
    res.sendFile('overpress_components/' + req.params[0], { root : __dirname });
});

router.get('/css/:filename', function (req, res) {
    res.sendFile('css/' + req.params.filename, { root : __dirname });
});

router.get('/js/:filename', function (req, res) {
    res.sendFile('js/' + req.params.filename, { root : __dirname });
});

router.get('/img/:filename', function (req, res) {
    res.sendFile('img/' + req.params.filename, { root : __dirname });
});

router.get(/^(.+)$/, function (req, res) {
    res.status(404).send('Sorry, we cannot find that!');
});

server = app.listen(port, function () {
    console.log('   info  - Server listening on port ' + port);
});

//
// Socket.IO Server
//

io = io(server, {log: false});

io.on('connection', function (server) {
    server.on('socketUpdateSlides', function (data) {
        if (data.id && data.slide) {
            //server.broadcast.emit(data);
            server.broadcast.emit('changeSlides', data);
            console.log('    info - socketUpdateSlides - id:' + data.id + ' - slide:' + data.slide);
        }
    });
});
/*
io.on("connection", function(client) {
    
    // Slide
    client.on("message", function(msg) {
      if (msg.slide) {
        client.broadcast.json.send(msg);	  	
      }
      if (msg.type === "send") {
        client.json.send( { "text": msg.text, "type":"send" } );
      } else if (msg.type === "broad") {
        client.broadcast.json.send( { "text": msg.text, "type":"broad" } );
      }
    });
    
    // Draw
    client.on("draw", function (data) {
        client.broadcast.emit("draw", data);
    });
    client.on("color", function (color) {
        client.broadcast.emit("color", color);
    });
    client.on("composite", function (composite) {
        client.broadcast.emit("composite", composite);
    });
    client.on("visibility", function (visibility) {
        client.broadcast.emit("visibility", visibility);
    });
    client.on("lineWidth", function (width) {
        client.broadcast.emit("lineWidth", width);
    });
    client.on("clear", function () {
        client.broadcast.emit("clear");
    });
});*/