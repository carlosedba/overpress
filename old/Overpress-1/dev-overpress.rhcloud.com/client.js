  // *************************************************** //
  // * Overpress Client Core *************************** //
  // * Alpha Stage ************************************* //
  // *************************************************** //
  // * SlideShow Base by: ****************************** //
  // * Marcin Wichary (mwichary@google.com) ************ //
  // * Ernest Delgado (ernestd@google.com) ************* //
  // * Alex Russell (slightlyoff@chromium.org) ********* //
  // * Brad Neuberg ************************************ //
  // * ************************************************* //
  // * Classie - Class Helper Functions by: ************ //
  // * David DeSandro (desandro.com) ******************* //
  // *************************************************** //
  // * Overpress Developed by: ************************* //
  // * Carlos Eduardo (carlosed_almeida@live.com) ****** //
  // *************************************************** //

  var Overpress = (function () {
      
      //Algumas variáveis e suas funções
      var doc = document,
          disableBuilds = true,
          ctr = 0,
          spaces = /\s+/,
          a1 = [''],
  
          toArray = function (list) {
              return Array.prototype.slice.call(list || [], 0);
          },
  
          byId = function (id) {
              if (typeof id === 'string') {
                  return doc.getElementById(id);
              }
              return id;
          },
  
          query = function (query, root) {
              if (!query) {
                  return [];
              }
              if (typeof query !== 'string') {
                  return toArray(query);
              }
              if (typeof root === 'string') {
                  root = byId(root);
                  if (!root) {
                      return [];
                  }
              }
  
              root = root || doc;
  
              var rootIsDoc = (root.nodeType === 9);
  
              // rewrite the query to be ID rooted
              if (!rootIsDoc || ('>~+'.indexOf(query.charAt(0)) >= 0)) {
                  root.id = root.id || ('qUnique' + (ctr));
                  query = '#' + root.id + ' ' + query;
              }
  
              // don't choke on something like ".yada.yada >"
              if ('>~+'.indexOf(query.slice(-1)) >= 0) {
                  query += ' *';
              }
              return toArray(doc.querySelectorAll(query));
          },
  
          strToArray = function (s) {
              if (typeof s === 'string' || s instanceof String) {
                  if (s.indexOf(' ') < 0) {
                      a1[0] = s;
                      return a1;
                  } else {
                      return s.split(spaces);
                  }
              }
              return s;
          },
  
          addClass = function (node, classStr) {
              classStr = strToArray(classStr);
              var cls = ' ' + node.className + ' ';
              for (var i = 0, len = classStr.length, c; i < len; ++i) {
                  c = classStr[i];
                  if (c && cls.indexOf(' ' + c + ' ') < 0) {
                      cls += c + ' ';
                  }
              }
              node.className = cls.trim();
          },
  
          removeClass = function (node, classStr) {
              var cls;
              if (classStr !== undefined) {
                  classStr = strToArray(classStr);
                  cls = ' ' + node.className + ' ';
                  for (var i = 0, len = classStr.length; i < len; ++i) {
                      cls = cls.replace(' ' + classStr[i] + ' ', ' ');
                  }
                  cls = cls.trim();
              } else {
                  cls = '';
              }
              if (node.className != cls) {
                  node.className = cls;
              }
          },
  
          toggleClass = function (node, classStr) {
              var cls = ' ' + node.className + ' ';
              if (cls.indexOf(' ' + classStr.trim() + ' ') >= 0) {
                  removeClass(node, classStr);
              } else {
                  addClass(node, classStr);
              }
          },
  
          ua = navigator.userAgent,
          isFF = parseFloat(ua.split('Firefox/')[1]) || undefined,
          isWK = parseFloat(ua.split('WebKit/')[1]) || undefined,
          isOpera = parseFloat(ua.split('Opera/')[1]) || undefined,
  
          canTransition = (function () {
  
              var ver = parseFloat(ua.split('Version/')[1]) || undefined;
              // Verifica se o navegador suporta Transições CSS
              var cachedCanTransition =
                  (isWK || (isFF && isFF > 3.6) || (isOpera && ver >= 10.5));
  
              return function () {
                  return cachedCanTransition;
              }
          })();
  
      //Classe Slide
      var Slide = function (node, idx) {
          this._node = node;
          if (idx >= 0) {
              this._count = idx + 1;
          }
          if (this._node) {
              addClass(this._node, 'slide distant-slide');
          }
          this._makeCounter();
          this._makeBuildList();
      };
      
      Slide.prototype = {
          _node: null,
          _count: 0,
          _buildList: [],
          _visited: false,
          _currentState: '',
          _states: ['distant-slide', 'far-past',
              'past', 'current', 'future',
              'far-future', 'distant-slide'
          ],
          
          setState: function (state) {
              if (typeof state != 'string') {
                  state = this._states[state];
              }
              if (state == 'current' && !this._visited) {
                  this._visited = true;
                  this._makeBuildList();
              }
              removeClass(this._node, this._states);
              addClass(this._node, state);
              this._currentState = state;
              var _t = this;
              setTimeout(function () {
                  _t._runAutos();
              }, 400);
          },
          
          _makeCounter: function () {
              if (!this._count || !this._node) {
                  return;
              }
              var c = doc.createElement('span');
              c.innerHTML = this._count;
              c.className = 'counter';
              this._node.appendChild(c);
          },
          
          _makeBuildList: function () {
              this._buildList = [];
              if (disableBuilds) {
                  return;
              }
              if (this._node) {
                  this._buildList = query('[data-build] > *', this._node);
              }
              this._buildList.forEach(function (el) {
                  addClass(el, 'to-build');
              });
          },
          
          _runAutos: function () {
              if (this._currentState != 'current') {
                  return;
              }
              var idx = -1;
              this._buildList.some(function (n, i) {
                  if (n.hasAttribute('data-auto')) {
                      idx = i;
                      return true;
                  }
                  return false;
              });
              if (idx >= 0) {
                  var elem = this._buildList.splice(idx, 1)[0];
                  var transitionEnd = isWK ? 'webkitTransitionEnd' : (isFF ? 'mozTransitionEnd' : 'oTransitionEnd');
                  var _t = this;
                  if (canTransition()) {
                      var l = function (evt) {
                          elem.parentNode.removeEventListener(transitionEnd, l, false);
                          _t._runAutos();
                      };
                      elem.parentNode.addEventListener(transitionEnd, l, false);
                      removeClass(elem, 'to-build');
                  } else {
                      setTimeout(function () {
                          removeClass(elem, 'to-build');
                          _t._runAutos();
                      }, 400);
                  }
              }
          },
          
          buildNext: function () {
              if (!this._buildList.length) {
                  return false;
              }
              removeClass(this._buildList.shift(), 'to-build');
              return true;
          },
      };
      
      //Classe SlideShow
      var SlideShow = function (slides) {
          this._slides = (slides || []).map(function (el, idx) {
              return new Slide(el, idx);
          });
          
          var h = window.location.hash;
          
          try {
              this.current = parseInt(h.split('#slide')[1], 10);
          } catch (e) {}
          
          this.current = isNaN(this.current) ? 1 : this.current;
          var _t = this;
          
          doc.addEventListener('keydown',
              function (e) {
                  _t.handleKeys(e);
              }, false);
          doc.addEventListener('mousewheel',
              function (e) {
                  _t.handleWheel(e);
              }, false);
          doc.addEventListener('DOMMouseScroll',
              function (e) {
                  _t.handleWheel(e);
              }, false);
          doc.addEventListener('touchstart',
              function (e) {
                  _t.handleTouchStart(e);
              }, false);
          doc.addEventListener('touchend',
              function (e) {
                  _t.handleTouchEnd(e);
              }, false);
          window.addEventListener('popstate',
              function (e) {
                  if (e.state) {
                      _t.go(e.state);
                  }
              }, false);
          
          this._update();
      };
      
      SlideShow.prototype = {
          _slides: [],
          _update: function (dontPush, dontSync) {
              if (history.pushState) {
                  if (!dontPush) {
                      history.replaceState(this.current, 'Slide ' + this.current, '#' + this.current);
                  }
              } else {
                  window.location.hash = '' + this.current;
              }
              for (var x = this.current - 1; x < this.current + 7; x++) {
                  if (this._slides[x - 4]) {
                      this._slides[x - 4].setState(Math.max(0, x - this.current));
                  }
              }
              // Socket.IO para sincronizar o Slide
              if (!dontSync && _id) {
                  socket.json.send({
                      "slide": this.current,
                      "id": _id
                  });
              }
          },
          
          current: 0,
          next: function () {
              if (!this._slides[this.current - 1].buildNext()) {
                  this.current = Math.min(this.current + 1, this._slides.length);
                  this._update();
              }
          },
          prev: function () {
              this.current = Math.max(this.current - 1, 1);
              this._update();
          },
          go: function (num, dontSync) {
              this.current = num;
              this._update(true, dontSync);
          },
          log: function () {
              console.log('logged!')
          },
          handleWheel: function (e) {
              var delta = 0;
              if (e.wheelDelta) {
                  delta = e.wheelDelta / 120;
                  if (isOpera) {
                      delta = -delta;
                  }
              } else if (e.detail) {
                  delta = -e.detail / 3;
              }
              if (delta > 0) {
                  this.prev();
                  return;
              }
              if (delta < 0) {
                  this.next();
                  return;
              }
          },
          handleKeys: function (e) {
              if (/^(input|textarea)$/i.test(e.target.nodeName)) return;
              switch (e.keyCode) {
              case 37: // seta esquerda
                  this.prev();
                  break;
              case 39: // seta direita
              case 32: // espaço
                  this.next();
                  break;
              }
          },
          _touchStartX: 0,
          handleTouchStart: function (e) {
              this._touchStartX = e.touches[0].pageX;
              e.preventDefault();
          },
          handleTouchEnd: function (e) {
              var delta = this._touchStartX - e.changedTouches[0].pageX;
              var SWIPE_SIZE = 150;
              if (delta > SWIPE_SIZE) {
                  this.next();
              } else if (delta < -SWIPE_SIZE) {
                  this.prev();
              }
              e.preventDefault();
          },
      };
      
      // Inicia a sincronização usando Socket.IO
      socket = io.connect();
      
      socket.on('message', function (msg) {
          if (msg.slide && msg.id === _id) {
              slideshow.go(msg.slide, true);
          }
          if (msg.type === "send") {
              document.getElementById("send-log").innerHTML = msg.text;
          } else if (msg.type === "broad") {
              document.getElementById("broad-log").innerHTML = msg.text;
          }
      });
      
      var parseQueryParams = function (queryString) {
          var match = queryString.match(/([^?#]*)(#.*)?$/);
          if (!match) return null;
          var queryParams = match[1].split("&");
          var id;
          for (var idx in queryParams) {
              var param = queryParams[idx];
              if (param.split("=")[0] === "id") {
                  id = param.split("=")[1];
              }
          }
          return id;
      },
      
         _id = parseQueryParams(window.location.search),
      
      // Inicia o SlideShow
          slideshow = new SlideShow(query('.slide'));
      
      // Draw
      
      window.addEventListener("load", function () {

      var canvas = document.getElementById("draw");
      var c = canvas.getContext("2d");
      var w = screen.width;
      var h = screen.height;
      var drawing = false;
      var oldPos;

      // Padrões
      canvas.width = w;
      canvas.height = h;
      c.strokeStyle = "#000";
      c.lineWidth = 5;
      c.lineJoin = "round";
      c.lineCap = "round";

      // Funções que calculam as coordenadas
      function scrollX(){
        return document.documentElement.scrollLeft || document.body.scrollLeft;
      }
      function scrollY(){
        return document.documentElement.scrollTop || document.body.scrollTop;
      }
      function getPos (event) {
        var mouseX = event.clientX - canvas.offsetLeft + scrollX();
        var mouseY = event.clientY - canvas.offsetTop + scrollY();
        return {x:mouseX, y:mouseY};
      }
      function getPosT (event) {
        var mouseX = event.touches[0].clientX - canvas.offsetLeft + scrollX();
        var mouseY = event.touches[0].clientY - canvas.offsetTop + scrollY();
        return {x:mouseX, y:mouseY};
      }
                                   
      // Event Listeners
          
      canvas.addEventListener("mousedown", function (event) {
        //console.log("mousedown");
        drawing = true;
        oldPos = getPos(event);
      }, false);
          
      canvas.addEventListener("mouseup", function () {
        //console.log("mouseup");
        drawing = false;
      }, false);
          
      canvas.addEventListener("mousemove", function (event) {
        var pos = getPos(event);
        //console.log("mousemove : x=" + pos.x + ", y=" + pos.y + ", drawing=" + drawing);
        if (drawing) {
          c.beginPath();
          c.moveTo(oldPos.x, oldPos.y);
          c.lineTo(pos.x, pos.y);
          c.stroke();
          c.closePath();
            
      //Socket.IO
            
          socket.emit("draw", {id:_id, before:oldPos, after:pos});
          oldPos = pos;
        }
      }, false);
          
      canvas.addEventListener("mouseout", function () {
        //console.log("mouseout");
        drawing = false;
      }, false);
          
     
      // A opção de cor e espessura selecionada é enviada para o Servidor
      var black = document.getElementById('black'),
          white = document.getElementById('white'),
          blue = document.getElementById('blue'),
          red = document.getElementById('red'),
          green = document.getElementById('green'),
          eraser = document.getElementById('eraser'),
          small = document.getElementById('small'),
          middle = document.getElementById('middle'),
          large = document.getElementById('large'),
          clear = document.getElementById('clear'),
          visibility = document.getElementById('visibility'),
          draw = document.getElementById('draw');
          
      black.onclick = function () {c.globalCompositeOperation = "copy";c.strokeStyle = "black";socket.emit("color", "black");socket.emit("composite", "copy");};
      white.onclick = function () {c.globalCompositeOperation = "copy";c.strokeStyle = "white";socket.emit("color", "white");socket.emit("composite", "copy");};
      blue.onclick = function () {c.globalCompositeOperation = "copy";c.strokeStyle = "blue";socket.emit("color", "blue");socket.emit("composite", "copy");};
      red.onclick = function () {c.globalCompositeOperation = "copy";c.strokeStyle = "red";socket.emit("color", "red");socket.emit("composite", "copy");};
      green.onclick = function () {c.globalCompositeOperation = "copy";c.strokeStyle = "green";socket.emit("color", "green");socket.emit("composite", "copy");};
      eraser.onclick = function () {c.globalCompositeOperation = "destination-out";c.strokeStyle = "rgba(0,0,0,1)";socket.emit("color", "rgba(0,0,0,1)");socket.emit("composite", "destination-out");};
      clear.onclick = function () {c.clearRect ( 0 , 0 , w , h );socket.emit("clear");};
      small.onclick = function () {c.lineWidth = 5;socket.emit("lineWidth", 5);};
      middle.onclick = function () {c.lineWidth = 10;socket.emit("lineWidth", 10);};
      large.onclick = function () {c.lineWidth = 20;socket.emit("lineWidth", 20);};
          
        //Display Toggle
        (function prepToggle () {draw.style.visibility = "hidden";})();
        visibility.onclick = function () {
            if (draw.style.visibility == "hidden") {draw.style.visibility = "visible";socket.emit("visibility", "visible");} else {draw.style.visibility = "hidden";socket.emit("visibility", "hidden");}
        };
         
     
      // Sincronização Socket.IO
      socket.on("draw", function (data) {
        //console.log("info - id : " + data.id);
        if (data.id === _id) {
        c.beginPath();
        c.moveTo(data.before.x, data.before.y);
        c.lineTo(data.after.x, data.after.y);
        c.stroke();
        c.closePath();
        }
      });
     
      socket.on("color", function (data) {
        //console.log("info - color : " + data);
        c.strokeStyle = data;
      });
      socket.on("composite", function (data) {
        //console.log("info - composite : " + data);
        c.globalCompositeOperation = data;
      });
      socket.on("visibility", function (data) {
        //console.log("info - visibility : " + data);
        draw.style.visibility = data;
      });
      socket.on("lineWidth", function (data) {
        //console.log("info - lineWidth : " + data);
        c.lineWidth = data;
      });
      socket.on("clear", function () {
        //console.log("info - draw cleaned");
        c.clearRect ( 0 , 0 , w , h );
      });

                               
    }, false);
      
      //Left Side Menu
      var barz = document.getElementById('barz'),
          lmenu = document.getElementsByClassName('lmenu'),
          navbar = document.getElementsByClassName('navbar'),
          x = lmenu[0],
          y = navbar[0];
      
      barz.onclick = function () {
          classie.toggle (this, 'active');
          classie.toggle (x, 'lmenu-open');
          classie.toggle (y, 'navbar-toright')
          disableBARZ ('active');
      };
      
      function disableBARZ (barz) {
          if (barz !== 'active') {
              classie.toggle (barz, 'disable');
          }
      }
      
      //Overpress API
      return {
          next: function () {
              slideshow.next();
          },
          prev: function () {
              slideshow.prev();
          },
      };
      
  })();