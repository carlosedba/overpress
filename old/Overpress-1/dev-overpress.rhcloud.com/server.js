#!/bin/env node

// *************************************************** //
// * Overpress Server Core *************************** //
// * Alpha Stage ************************************* //
// * Developed by Carlos Eduardo Barbosa de Almeida  * //
// *************************************************** //

// Set up app variables
var express = require('express'),
    io = require("socket.io"),
    app = express(),
    ipaddr  = process.env.OPENSHIFT_NODEJS_IP,
    port    = process.env.OPENSHIFT_NODEJS_PORT || 8080;

/*  =====================================================================  */
/*  Setup route handlers.  */
/*  =====================================================================  */

app.get('/', function (req, res) {
    res.sendfile('index.html');
});

app.get('/slider', function (req, res) {
    res.sendfile('slider.html');
});

app.get('/powerpoint', function (req, res) {
    res.sendfile('powerpoint.html');
});

app.get(/^(.+)$/, function (req, res) {
    res.sendfile(__dirname + req.params[0]);
});

//  Get the environment variables we need.

if (typeof ipaddr === "undefined") {
   console.warn('No OPENSHIFT_NODEJS_IP environment variable');
}

//  terminator === the termination handler.
function terminator(sig) {
   if (typeof sig === "string") {
      console.log('%s: Received %s - terminating Node server ...',
                  Date(Date.now()), sig);
      process.exit(1);
   }
   console.log('%s: Node server stopped.', Date(Date.now()) );
}

//  Process on exit and signals.
process.on('exit', function() { terminator(); });

['SIGHUP', 'SIGINT', 'SIGQUIT', 'SIGILL', 'SIGTRAP', 'SIGABRT', 'SIGBUS',
 'SIGFPE', 'SIGUSR1', 'SIGSEGV', 'SIGUSR2', 'SIGPIPE', 'SIGTERM'
].forEach(function(element, index, array) {
    process.on(element, function() { terminator(element); });
});

//  And start the app on that interface (and port).
server = app.listen(port, ipaddr, function() {
   console.log('%s: Node server started on %s:%d ...', Date(Date.now() ),
               ipaddr, port);
    
    //Socket.IO Server
    io = io.listen(server, {log: false});
    
    io.sockets.on("connection", function(client) {
    
    //Slide
    client.on("message", function(msg) {
      if (msg.slide) {
        client.broadcast.json.send(msg);	  	
      }
      if (msg.type === "send") {
        client.json.send( { "text": msg.text, "type":"send" } );
      } else if (msg.type === "broad") {
        client.broadcast.json.send( { "text": msg.text, "type":"broad" } );
      }
    });
    
    //Draw
    client.on("draw", function (data) {
        client.broadcast.emit("draw", data);
    });
    client.on("color", function (color) {
        client.broadcast.emit("color", color);
    });
    client.on("composite", function (composite) {
        client.broadcast.emit("composite", composite);
    });
    client.on("visibility", function (visibility) {
        client.broadcast.emit("visibility", visibility);
    });
    client.on("lineWidth", function (width) {
        client.broadcast.emit("lineWidth", width);
    });
    client.on("clear", function () {
        client.broadcast.emit("clear");
    });
});
});