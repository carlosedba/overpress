// *************************************************** //
// * Overpress Server Core *************************** //
// * Alpha Stage ************************************* //
// * Developed by Carlos Eduardo Barbosa de Almeida  * //
// *************************************************** //

var express = require('express'),
    app = express(),
    router = express.Router(),
    io = require("socket.io"),
    fs = require('fs'),
    path = require('path'),
    port = 1000;

// Routes

app.use('/', router);
app.set('views', path.join( __dirname, '/views'));
app.set('view engine', 'vash');

router.use(function (req, res, next) {
    console.log(req.method, req.url);
    next();
});

router.get('/', function (req, res) {
    res.sendfile('index.html');
});

router.get(/^(.+)$/, function (req, res) {
    res.sendfile(__dirname + req.params[0]);
});

server = app.listen(port, function () {
    console.log('   info  - Server listening on port ' + port);
});

// Socket.IO Server
io = io(server, {log: false});

io.on("connection", function(client) {
    
    // Slide
    client.on("message", function(msg) {
      if (msg.slide) {
        client.broadcast.json.send(msg);	  	
      }
      if (msg.type === "send") {
        client.json.send( { "text": msg.text, "type":"send" } );
      } else if (msg.type === "broad") {
        client.broadcast.json.send( { "text": msg.text, "type":"broad" } );
      }
    });
    
    // Draw
    client.on("draw", function (data) {
        client.broadcast.emit("draw", data);
    });
    client.on("color", function (color) {
        client.broadcast.emit("color", color);
    });
    client.on("composite", function (composite) {
        client.broadcast.emit("composite", composite);
    });
    client.on("visibility", function (visibility) {
        client.broadcast.emit("visibility", visibility);
    });
    client.on("lineWidth", function (width) {
        client.broadcast.emit("lineWidth", width);
    });
    client.on("clear", function () {
        client.broadcast.emit("clear");
    });
});