var Overpress = (function () {
	'use strict';

	var doc = document,
		OverElement = doc.registerElement('app-overpress', { prototype: Object.create(HTMLElement.prototype) }),
        SlidesElement = doc.registerElement('app-slides', { prototype: Object.create(HTMLElement.prototype) }),
        SlideElement = doc.registerElement('app-slide', { prototype: Object.create(HTMLElement.prototype) }),
	    slides = doc.querySelectorAll('app-slide'),
	    totalSlides = slides.length;

	function slideNumQuery (condition) {
		var selector = '[data-slide-num="' + condition + '"]',
		    query = doc.querySelector(selector);
		return query;
	}

	function buildSlides () {
		for (var x=0; x<totalSlides; x++) {
			var y = x + 1;
			slides[x].dataset.slideNum = y;
		};
	}
	
	function clearTimes() {
		for (var x=1; x<=totalSlides; x++) {
			var element = slideNumQuery(x);
			element.classList.remove('far-prev', 'prev', 'current', 'next', 'far-next');
		};
	}

	function updateSlides (num) {
		console.log('Log - updateSlides:');
		clearTimes();
		for (var x=num-2; x<=num+2; x++) {
			switch (x) {
				case num - 2:
				   if (num-2>0) {
				   	var element = slideNumQuery(num-2);
				   	element.classList.add('far-prev');
				   	console.log('far-prev');
				   };
				   break;
				case num - 1:
				   if (num-1>0) {
				   	var element = slideNumQuery(num-1);
				   	element.classList.add('prev');
				   	console.log('prev');
				   };
				   break;
				case num:
				   if (num>0) {
				   	var element = slideNumQuery(num);
				   	element.classList.add('current');
				   	console.log('current');
				   };
				   break;
				case num + 1:
				   if (num+1<=totalSlides) {
				   	var element = slideNumQuery(num+1);
				   	element.classList.add('next');
				   	console.log('next');
				   };
				   break;
				case num + 2:
				   if (num+2<=totalSlides) {
				   	var element = slideNumQuery(num+2);
				   	element.classList.add('far-next');
				   	console.log('far-next');
				   };
				   break;
			};
		};
	}

	// BootUp!
	buildSlides();
	updateSlides(1);

})();