var Overpress = (function () {
	'use strict';

	var doc = document,
		OverElement = doc.registerElement('app-overpress', { prototype: Object.create(HTMLElement.prototype) }),
        SlidesElement = doc.registerElement('app-slides', { prototype: Object.create(HTMLElement.prototype) }),
        SlideElement = doc.registerElement('app-slide', { prototype: Object.create(HTMLElement.prototype) }),
	    slides = doc.querySelectorAll('app-slide'),
	    totalSlides = slides.length;

	function slideNumQuery (condition) {
		var selector = '[data-slide-num="' + condition + '"]',
		    query = doc.querySelector(selector);
		return query;
	}

	function buildSlides () {
		for (var x=0; x<totalSlides; x++) {
			var y = x + 1;
			slides[x].dataset.slideNum = y;
		};
	}
	
	function clearTimes() {
		for (var x=1; x<=totalSlides; x++) {
			var element = slideNumQuery(x);
			element.classList.remove('far-past', 'past', 'current', 'future', 'far-future');
		};
	}

	function updateSlides (current) {
		console.log('Remover:');
		for (var x=current-3; x<=current+1; x++) {
			switch (x) {
				case current - 3:
				   if (current-3>0) {
				   	var element = slideNumQuery(current-3);
				   	element.classList.remove('far-past');
				   	console.log('far-past');
				   };
				   break;
				case current - 2:
				   if (current-2>0) {
				   	var element = slideNumQuery(current-2);
				   	element.classList.remove('past');
				   	console.log('past');
				   };
				   break;
				case current - 1:
				   if (current-1>0) {
				   	var element = slideNumQuery(current-1);
				   	element.classList.remove('current');
				   	console.log('current');
				   };
				   break;
				case current:
				   if (current<=totalSlides) {
				   	var element = slideNumQuery(current);
				   	element.classList.remove('future');
				   	console.log('future');
				   };
				   break;
				case current + 1:
				   if (current+1<=totalSlides) {
				   	var element = slideNumQuery(current+1);
				   	element.classList.remove('far-future');
				   	console.log('far-future');
				   };
				   break;
			};
		};
		console.log('Acrescentar:');
		for (var x=current-2; x<=current+2; x++) {
			switch (x) {
				case current - 2:
				   if (current-2>0) {
				   	var element = slideNumQuery(current-2);
				   	element.classList.add('far-past');
				   	console.log('far-past');
				   };
				   break;
				case current - 1:
				   if (current-1>0) {
				   	var element = slideNumQuery(current-1);
				   	element.classList.add('past');
				   	console.log('past');
				   };
				   break;
				case current:
				   if (current>0) {
				   	var element = slideNumQuery(current);
				   	element.classList.add('current');
				   	console.log('current');
				   };
				   break;
				case current + 1:
				   if (current+1<=totalSlides) {
				   	var element = slideNumQuery(current+1);
				   	element.classList.add('future');
				   	console.log('future');
				   };
				   break;
				case current + 2:
				   if (current+2<=totalSlides) {
				   	var element = slideNumQuery(current+2);
				   	element.classList.add('far-future');
				   	console.log('far-future');
				   };
				   break;
			};
		};
	}

	// BootUp!
	buildSlides();
	updateSlides(3);

})();