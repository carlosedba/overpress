// *************************************************** //
// * Overpress ClientWorks *************************** //
// * Alpha Stage ************************************* //
// * Developed by ************************************ //
// * Carlos Eduardo (carlosed_almeida@Live.com) ****** //
// * Some concepts taken from Google I/O 2012 Slides * //
// *************************************************** //

var Overpress = (function () {
	'use strict';

	var doc = document,
		OverElement = doc.registerElement('app-overpress', { prototype: Object.create(HTMLElement.prototype) }),
        SlidesElement = doc.registerElement('app-slides', { prototype: Object.create(HTMLElement.prototype) }),
        SlideElement = doc.registerElement('app-slide', { prototype: Object.create(HTMLElement.prototype) }),
	    slides = doc.querySelectorAll('app-slide'),
	    totalSlides = slides.length,
	    API = {
	    	goTo: function (num) {
	    		updateSlides(num);
	    	},
	    	next: function () {
	    	    var currentSlide = parseInt(document.location.hash.substr(1));
	    		updateSlides(currentSlide + 1);
	    	},
	    	prev: function () {
	    		var currentSlide = parseInt(document.location.hash.substr(1));
	    		updateSlides(currentSlide - 1);
	    	}
	    };

	function slideNumQuery (condition) {
		var selector = '[data-num="' + condition + '"]',
		    query = doc.querySelector(selector);
		return query;
	}

	function buildSlides () {
		for (var x=0; x<totalSlides; x++) {
			var y = x + 1;
			slides[x].dataset.num = y;
		};
	}
	
	function clearTimes () {
		for (var x=1; x<=totalSlides; x++) {
			var element = slideNumQuery(x);
			element.classList.remove('far-prev', 'prev', 'current', 'next', 'far-next');
		};
	}

	function updateHistory (num, dontPush) {
		if (!dontPush) {
			var hash = '#' + num;
			if (window.history.pushState) {
				window.history.pushState(num, 'Slide ' + num, hash);
			} else {
				window.location.replace(hash);
			};
		};
	}

	function updateSlideFromHash () {
		var hash = parseInt(document.location.hash.substr(1));
		if (hash) {
			updateSlides(hash, true);
		} else {
			updateSlides(1, false);
		};
	}

	function updateSlides (num, push) {
		if (num > 0 && num <= totalSlides) {
			var dontPush = push;

			console.log('Log - updateSlides:');
			clearTimes();

			for (var x=num-2; x<=num+2; x++) {
				switch (x) {
					case num - 2:
					  if (num-2>0) {
						var element = slideNumQuery(num-2);
						element.classList.add('far-prev');
						console.log('far-prev');
				      };
				    break;
				    case num - 1:
				      if (num-1>0) {
				      	var element = slideNumQuery(num-1);
				      	element.classList.add('prev');
				      	console.log('prev');
				      };
				    break;
				    case num:
				      if (num>0) {
				      	var element = slideNumQuery(num);
				        element.classList.add('current');
				        console.log('current');
				      };
				    break;
				    case num + 1:
				      if (num+1<=totalSlides) {
				      	var element = slideNumQuery(num+1);
				      	element.classList.add('next');
				      	console.log('next');
				      };
				    break;
				    case num + 2:
				      if (num+2<=totalSlides) {
				      	var element = slideNumQuery(num+2);
				     	element.classList.add('far-next');
				     	console.log('far-next');
				      };
				   break;
				};
			};
			updateHistory(num, dontPush);
		};
	}

	function eventListeners () {
		window.addEventListener('popstate', function (e) {
			if (e.state != null) {
				updateSlides(e.state, true);
				e.preventDefault();
			} else {
				updateSlideFromHash();
			};
		}, false);	
		doc.addEventListener('keydown', function (e) {
			switch (e.keyCode) {
				case 38:
				case 39:
				  API.next();
				break;
				case 40:
				case 37:
				  API.prev();
				break;
			};
		}, false);
	}

	var canvas =  doc.getElementById("draw"),
	    c = canvas.getContext("2d"),
	    w = screen.width,
	    h = screen.height,
	    drawing = false,
	    oldPos;

	canvas.width = w;
	canvas.height = h;
	c.strokeStyle = "#000";
	c.lineWidth = 5;
	c.lineJoin = "round";
	c.lineCap = "round";

	function scrollX () {
		return doc.documentElement.scrollLeft || doc.body.scrollLeft;
	}

	function scrollY () {
		return document.documentElement.scrollTop || document.body.scrollTop;
	}

	function getPos (event) {
		var mouseX = event.clientX - canvas.offsetLeft + scrollX(),
		    mouseY = event.clientY - canvas.offsetTop + scrollY();
		return {x:mouseX, y:mouseY};
	}

	function getPosT (event) {
		var mouseX = event.touches[0].clientX - canvas.offsetLeft + scrollX(),
		    mouseY = event.touches[0].clientY - canvas.offsetTop + scrollY();
		return {x:mouseX, y:mouseY};
	}

	// BootUp!
	buildSlides();
	eventListeners();
	updateSlideFromHash();

	// API
	return API;

})();