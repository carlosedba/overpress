<?php
// Dispatch Framework

// Arquivos Inclusos
require 'app/includes/dispatch.php';
require 'app/includes/functions.php';

// Arquivo de configuração
config('source', 'app/config.ini');

// Isso atingirá a Index
get('/index', function () {

	$pagina = from($_GET, 'pagina');
	$pagina = $pagina ? (int)$pagina : 1;
	
	$slides = get_sli($pagina); 
	
	if(empty($slides) || $pagina < 1){
		// 404
		nada_encontrado();
	}
	
    render('lista',array(
    	'pagina' => $pagina,
		'slidesl' => $slides,
		'tem_pagina' => tem_pagina($pagina)
	));
});

// Gera a página do Slide
get('/:name',function($nome){

	$slide = p_arquivo($nome);

	if(!$slide){
		nada_encontrado();
	}

	render('slide',array(
		'titulo' => $slide->titulo .' - ' . config('blog.title'),
		'p' => $slide
	));
});

// Se nada dor encontrado
// envia-se o erro 404

get('.*',function(){
	nada_encontrado();
});

// Finish RIM!
dispatch();
