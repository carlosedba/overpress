<?php


/* Funções Gerais */

function get_sli_files(){

	static $_cache = array();

	if(empty($_cache)){

		// Pega o nome dos arquivos
		// de todos os slides:

		$_cache = array_reverse(glob('slides/*.sli'));
	}

	return $_cache;
}

function get_sli($pagina = 1, $porpagina = 0){

	if($porpagina == 0){
		$porpagina = config('arquivos.porpagina');
	}

	$slides = get_sli_files();

	// Extrai resultados específicos
	$slides = array_slice($slides, ($pagina -1) * $porpagina, $porpagina);

	$tmp = array();
	
	foreach($slides as $k=>$v){

		$slide = new stdClass;

		// Novo método de leitura de nomes
		$ext = pathinfo($v, PATHINFO_EXTENSION);
		$arr = basename($v, ".".$ext);

		// Lê a partir do _
		//$arr = explode('_', $v);

		// Re-escreve a URL
		$slide->url = site_url().str_replace('.sli','',$arr);

		// Get the contents and convert it to HTML
		$content = file_get_contents($v);

		// Extrai o titulo e o body
		$arr = explode('</titulo>', $content);
		$slide->titulo = str_replace('<titulo>','',$arr[0]);
		$slide->body = $arr[1];

		$tmp[] = $slide;
	}

	return $tmp;
}

// Procura o arquivo por nome
function p_arquivo($nome){

	foreach(get_sli_files() as $index => $v){
		if( strpos($v, $nome.'.sli') !== false){

			// Usa o método get_sli para retornar 
			// o arquivo corretamente

			$arr = get_sli($index+1,1);
			return $arr[0];
		}
	}

	return false;
}

// Vefifica se é necessário adicionar paginação
function tem_pagina($pagina = 1){
	$total = count(get_sli_files());

	return array(
		'prev'=> $pagina > 1,
		'next'=> $total > $pagina*config('arquivos.porpagina')
	);
}

// O erro 404
function not_found(){
	error(404, render('404', null, false));
}