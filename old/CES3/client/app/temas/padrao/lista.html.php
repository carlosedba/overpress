        <style type="text/css">
  .tabela {
  position: relative;
  padding: 45px 15px 15px;
  margin: 0 -15px 15px;
  background-color: #fafafa;
  box-shadow: inset 0 3px 6px rgba(0,0,0,.05);
  border-color: #e5e5e5 #eee #eee;
  border-style: solid;
  border-width: 1px 0;
  margin-left: 100px;
  margin-top: 100px;
  width: 400px;
} 
.tabela:after {
  content: "Lista de Slides";
  position: absolute;
  top:  15px;
  left: 15px;
  font-size: 12px;
  font-weight: bold;
  color: #bbb;
  text-transform: uppercase;
  letter-spacing: 1px;
}
     </style>
        <div class="tabela">
        <table class="table table-striped">
		<thead>
			<tr><th>Nome</th></tr>
		</thead>
		<tbody>
	    <?php foreach($slidesl as $p):?>
		<tr><td><a href="<?php echo $p->url?>"><?php echo $p->titulo ?></a></td></tr>
		<?php endforeach;?>
		</tbody>
	    </table>
<?php if ($tem_pagina['prev']):?>
	<a href="?pagina=<?php echo $pagina-1?>">Próxima</a>
<?php endif;?>

<?php if ($tem_pagina['next']):?>
	<a href="?pagina=<?php echo $pagina+1?>">Voltar</a>
<?php endif;?>
	    </div>
