<!DOCTYPE html>
<html manifest='cache.manifest' class='no-js'> 
<head>
<meta charset='utf-8' />
<meta http-equiv='X-UA-Compatible' content='IE=Edge;chrome=1' />
    <title>Lista de Slides | CESlides System</title>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
    <link rel='stylesheet' href='bootstrap-3.0.3-dist/css/bootstrap.min.css' />
    <link rel="stylesheet" href="font-awesome-4.0.3/css/font-awesome.min.css">
    <link rel="stylesheet" href="app/css/base.css">

</head>
<body>
       <h1 class="titulo">Redirecionando...</h1>
       <?php
       require 'fconfig.php';
       config('source', 'config.ini');
       $id = $_POST["id"];
       $ip = config('server-ip');
       header("Location: http://" . $ip . ":8181?id=" . $id);
       ?>
        <h3 class="creditos">Desenvolvido por Carlos Eduardo.</h3>
</body>
</html>