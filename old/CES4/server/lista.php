<!DOCTYPE html>
<html manifest='cache.manifest' class='no-js'>
<head>
<meta charset='utf-8' />
<meta http-equiv='X-UA-Compatible' content='IE=Edge; chrome=1; text/html; charset=utf-8' />
    <title>Lista de Slides | CESlides System</title>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
    <link rel='stylesheet' href='bootstrap-3.0.3-dist/css/bootstrap.min.css' />
    <link rel="stylesheet" href="font-awesome-4.0.3/css/font-awesome.min.css">
    <link rel="stylesheet" href="app/css/base.css">
    <script src='bootstrap-3.0.3-dist/js/jquery-1.10.2.min.js'></script>
    <script type="text/javascript">
$(document).ready(function(){
  // Write on keyup event of keyword input element
  $("#busca").keyup(function(){
    // When value of the input is not blank
        var term=$(this).val()
    if( term != "")
    {
      // Show only matching TR, hide rest of them
      $("#table tbody>tr").hide();
            $("#table td").filter(function(){
                   return $(this).text().toLowerCase().indexOf(term ) >-1
            }).parent("tr").show();
    }
    else
    {
      // When there is no input or clean again, show everything back
      $("#table tbody>tr").show();
    }
  });
});
    </script>
</head>
<body>

    <h1 class="titulo">Lista de Slides:</h1>
    <div class="musicas">
      <div class="input-group input-group" id="buscapn">
       <span class="input-group-addon" id="busbtn"><i class="fa fa-search"></i> Busca</span>
        <input type="text" class="form-control" id="busca" placeholder="Nome...">
      </div>
    <table class="table" id="table">
        <thead>
          <tr>
            <th>Nome:</th>
          </tr>
        </thead>
        <tbody>
          <?php
    // Muda codificação para aceitar acentos
    header('Content-Type: text/html; charset=ISO-8859-1',true); 
    // Pega os arquivos .sli da pasta /slides e coloca na string $arquivo
    foreach (glob("slides/*.sli") as $arquivo) {

        // Inicia a leitura do conteúdo dos arquivos .sli
        $conteudo = file_get_contents($arquivo);
        
        //Cria uma nova variável vazia
        $slide = new stdClass;

        //Gera o nome dos arquivos .sli
        $ext = pathinfo($arquivo, PATHINFO_EXTENSION);
        $arqn = basename($arquivo, ".".$ext);

    echo "
          <tr>
           <td><a href='gerador_tmp.php?arquivo=$arqn.sli' class='name' target='_blank'>$arqn</a></td>
          </tr>

    ";

}
          ?>
        </tbody>
      </table>
    </div>
      <h3 class="creditos">Desenvolvido por Carlos Eduardo.</h3>
    <script src='bootstrap-3.0.3-dist/js/bootstrap.min.js'></script>

</body>
</html>