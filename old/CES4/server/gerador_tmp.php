<?php
        
        //Recebe a informação do slide escolhido
        $SLIes = $_GET["arquivo"];

        // Abre as bases de gravação
        $inicio = file_get_contents('bgr/inicio.bgr');
        $fim = file_get_contents('bgr/fim.bgr');

        //Abre o slide especificado
        $as = file_get_contents('slides/' . $SLIes);
        //$as = file_get_contents('slides/HU3BR.sli');

        // Abre e grava o arquivo temporário
        $tmpSLI = fopen('tmpSLI.html', 'r+');
        
        $gr = new stdClass;
        $gr = fwrite($tmpSLI, "$inicio $as $fim");

        fclose($tmpSLI);

?>
<!DOCTYPE html>
<html manifest='cache.manifest' class='no-js'> 
<head>
<meta charset='utf-8' />
<meta http-equiv='X-UA-Compatible' content='IE=Edge;chrome=1' />
    <title>ID | CESlides System</title>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
    <link rel='stylesheet' href='bootstrap-3.0.3-dist/css/bootstrap.min.css' />
    <link rel="stylesheet" href="font-awesome-4.0.3/css/font-awesome.min.css">
    <link rel="stylesheet" href="app/css/base.css">

</head>
<body> 

       <a href="lista.php"><img src="app/img/bvoltar.png" class="bvoltar"></a>
       <h1 class="tituloid">Digite sua ID de sincroniza&ccedil;&atilde;o:</h1>
       <div class="input-group input-group-lg" id="idi">
       <span class="input-group-addon"><i class="fa fa-link"></i></span>
<form action="idredir.php" method="post" style="height: 1px;">
       <input type="text" name="id" class="form-control" placeholder="ID" style="height: 46px;">
       </div>
       <button type="submit" class="btn btn-lg btn-success" id="idsub"><i class="fa fa-arrow-right"></i></button>
</form>

      <h3 class="creditos">Desenvolvido por Carlos Eduardo.</h3>

</body>
</html>